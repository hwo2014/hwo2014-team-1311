﻿using System;

namespace teamgsn
{
	public enum TurboOptions {
		None,
		Fire
	}

	public class Turbo : ISerialize	{
		public TurboOptions value;

		public Turbo () {
			value = TurboOptions.None;
		}

		public Turbo (TurboOptions val) {
			value = val;
		}

		public string Serialize(int gameTick) {
			Msg msg = new Msg ("turbo", "whoooooooooosh");
			return JsonHelper.Serialize(msg);
		}
	}
}

