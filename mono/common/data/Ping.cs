using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class Ping: ISerialize {
		public string Serialize(int gameTick) {
			Msg msg = new Msg ("ping", this);
			return JsonHelper.Serialize(msg);
		}
	}
}

