using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class BotId {
		public string name;
		public string key;

		public static readonly BotId GSN_BOT_ID = new BotId ("Team GSN", "ER+v1DkiQsn5qg");

		public BotId(string name, string key) {
			this.name = name;
			this.key = key;
		}
	}
}

