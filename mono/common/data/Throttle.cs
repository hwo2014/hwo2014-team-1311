using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class Throttle : ISerialize {
		public float value;

		public Throttle(float value) {
			this.value = value;
		}

		public string Serialize(int gameTick) {
			TickMsg msg = new TickMsg ("throttle", value, gameTick);
			return JsonHelper.Serialize(msg);
		}
	}
}

