using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public interface ISerialize {
		string Serialize(int gameTick);
	}
}

