using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class Join: ISerialize {
		public string name;
		public string key;
		public string color;

		public Join(string name, string key) {
			this.name = name;
			this.key = key;
			this.color = "red";
		}
		
		public string Serialize(int gameTick) {
			Msg msg = new Msg ("join", this);
			return JsonHelper.Serialize(msg);
		}
	}
}

