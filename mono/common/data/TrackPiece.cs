﻿using System;
using Newtonsoft.Json;

namespace teamgsn 
{
	public class TrackPiece {
		public float length;

		[JsonProperty("switch")]
		public bool doesSwitch;	// is just called "switch" in the data
		public float angle;
		public float radius;

		public TrackPiece (float len, bool s = false, float ang = 0) {
			length = len;
			doesSwitch = s;
			angle = ang;
		}

		public bool isTurn {
			get {
				return !RaceMath.fuzzyZero (angle);
			}
		}
	}
}

