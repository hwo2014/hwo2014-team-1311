using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class CarPosition {
		public CarID id;
		public float angle;
		public PiecePosition piecePosition;

		public CarPosition(float angle) {
			this.angle = angle;
		}
	}
}

