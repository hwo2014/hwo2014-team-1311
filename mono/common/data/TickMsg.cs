using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class TickMsg : Msg {

		public static readonly int DEFAULT_GAME_TICK = -1;
		public int gameTick = DEFAULT_GAME_TICK;

		public TickMsg(string msgType, Object data, int gameTick) : base(msgType, data) {
			this.gameTick = gameTick;
		}

		public bool hasGameTick { get { return gameTick != DEFAULT_GAME_TICK; } }
	}
}

