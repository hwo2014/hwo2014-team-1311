using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class GameMsg : TickMsg {
		public static readonly string DEFAULT_GAME_ID = string.Empty;
		public string gameId = DEFAULT_GAME_ID;

		public GameMsg(string msgType, Object data, string gameId, int gameTick) : base(msgType, data, gameTick) {
			this.gameId = gameId;
		}

		public bool hasGameId { get { return gameId != DEFAULT_GAME_ID; } }
	}
}

