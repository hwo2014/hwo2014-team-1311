using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public enum SwitchLanes {
		None,
		Left,
		Right
	}

	public class SwitchLane : ISerialize {
		public SwitchLanes value;
		
		public SwitchLane() {
			this.value = SwitchLanes.None;
		}

		public SwitchLane(SwitchLanes value) {
			this.value = value;
		}

		public string Serialize(int gameTick) {
			string valueStr;
			switch (value) {
			case SwitchLanes.Left:
				valueStr = "Left";
				break;
			case SwitchLanes.Right:
				valueStr = "Right";
				break;
			default:
				throw new Exception ("Invalid Lane Switch being sent");
			}
			 
			TickMsg msg = new TickMsg ("switchLane", valueStr, gameTick);
			return JsonHelper.Serialize(msg);
		}
	}
}

