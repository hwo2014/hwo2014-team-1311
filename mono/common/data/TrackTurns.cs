﻿using System;

namespace teamgsn
{
	public enum TrackTurns
	{
		/*
		 * A hairpin turn is often found at the end of a long straight, forcing a driver to
		 * slow drastically after a long high speed run
		 * left side and turn left for 225 degrees, then exit through the final 45 degree right hand turn.
		 * [-1?] Straight
		 * [0-3] Turns ~Same Radius ~Same Angle
		 * [4] Turn Same Direction
		 * [5] Turn Oppisite Direction
		 * [6?] Straight
		 */
		HairPin,
		/* 
		 * A second type of Hairpin turn featuring a decreasing radius entry and expanding radius exit is shown below.
		 * [-1?] Striaght
		 * [0-3] Decreasing Radius ~180 Angle
		 * [4?] Striaght
		 */
		HairPinAngle180Decreasing,
		/* 
		 * A second type of Hairpin turn featuring a decreasing radius entry and expanding radius exit is shown below.
		 * [-1?] Striaght
		 * [0-3] Increasing Radius ~180 Angle
		 * [4?] Striaght
		 */
		HairPinAngle180Increasing,
		/* 
		 * normally a constant radius curve that joins two fast straights. Cars brake well before the entry and
		 * then accelerate through the turn to the exit, but unlike the hairpin turn described earlier, this turn 
		 * employs larger radius curve sections to allow cars to negotiate it at a much higher rate of speed.
		 * [-1?] Striaght
		 * [0-3] ~Same Radius ~180 Angle
		 * [4?] Striaght
		 */
		Angle180,
		/* 
		 * same as Angle180
		 * [-1?] Striaght
		 * [0-1] ~Same Radius ~90 Angle
		 * [2?] Striaght
		 */
		Angle90,
		/*
		* An increasing radius parabolic turn requires a low entry speed and a progressively faster exiting velocity. 
		* [-1?] Striaght
		* [0-3] Increasing Radius Increasing Angle
		* [4?] Striaght
		*/
		ParabolicIncreasing,
		/*
		 * The decreasing radius parabolic turn has exactly the opposite consequences, here the car can enter 
		 * at a higher rate of speed, but must progressively slow as it goes deeper into the turn.
		* [-1?] Striaght
		* [0-3] Decreasing Radius Decreasing Angle
		* [4?] Striaght
		 */
		ParabolicDecreasing,
		/*
		* This type of corner is usually a constant radius turn comprised of the largest turn radius sizes available. 
		* The 4-lane example below uses 15" and 18" radius turns for all sections of the corner.
		* In the illustration below cars would enter from the left side and exit at the right.
		* 
		* Carousel turns provide an excellent means of connecting two long straights without losing too much speed in the turn itself. 
		* [-1?] Striaght
		* [0-4] ~Same Radius ~90 Angle
		* [5] Striaght
		* [6] ~Same Radius Oppisite Direction
		* [7?] Striaght
		*/
		Carousel,
		/* 
		 * By placing a short straight section between two 45 degree turn sections you can create a turn that is much more 
		 * interesting and challenging to drive than a standard 90 degree left or right hander.
		 * 
		 * The overall effect of using a sweeping turn is to create two individual and distinct turns out of a the single 90 degree corner. 
		 * For added variety you can use different turn radius sizes for the entry and exit as well.
		 * 
		 * The dynamics of a changing radius sweeper are the same as those for the Parabolic turn described earlier. If the exit leads
		 * onto a fast portion of the track put the larger radius turns at the exit.
		 * [-1?] Striaght
		 * [0-4] ~Same Radius ~45 Angle
		 * [5] Striaght
		 * [6] ~Same Radius ~45 Angle
		 * [7?] Striaght
		 */  
		Sweep,
		/*
		* [-1?] Striaght
		* [0-4] ~Same Radius ~45 Angle
		* [5] Striaght
		* [6] ~Same Radius ~45 Angle Increasing Radius
		* [7?] Striaght
		*/  
		SweepIncreasing,
		/*
		* [-1?] Striaght
		* [0-4] ~Same Radius ~45 Angle
		* [5] Striaght
		* [6] ~Same Radius ~45 Angle Decreasing Radius
		* [7?] Striaght
		*/  
		SweepDecreasing,
		/* 
		 * The Kink is a gentle bend placed somewhere in the middle of a long straight. A kink forces a driver to lift slightly and 
		 * then get right back on the power. This type of turn, while deceptively simple can test a driver's skill.
		 * 
		 * Kinks are normally found towards the middle of long, fast straights. Try to make the overall turn radius as gentle as possible
		 * so that cars can maintain a high rate of speed both on entry and exit. The idea here is to make the driver lift slightly
		 * and then get right back on the throttle. No braking is done for this type of turn, other than the slight slowing of the motor. 
		 * [-1?] Striaght
		 * [0] Turn
		 * [1?] Striaght
		 */ 
		Kink,
		/* 
		 * A Chicane is normally inserted into a fast straight to slow cars down.
		 * 
		 * Unlike the kink described above, a chicane has a distinct braking zone prior to the entry apex,
		 * followed by a steady throttle section leading up to the exit apex. A driver can not get back on
		 * the throttle until the exit apex is reached, making the chicane a challenging turn on any HO scale race track.
		 * 
		 * The entry and exit speeds for a chicane can be manipulated by the careful choice of the
		 * turn radius sizes you select. In the illustration above both the entry and exit sections are 
		 * the same radius, but this could just as easily be adjusted to create a slower entry and faster exit 
		 * with decreasing and increasing radius sections respectively
		 * 
		 * A short 3" or 6" straight section of can also be added at the center of the chicane. This would prolong 
		 * the steady throttle section of the turn, making it even more challenging to drive.
		 * [-1?] Striaght
		 * [0] ~Same Angle 
		 * [1] ~Same Angle Oppisite Direction
		 * [2?] Striaght
		 */
		Chicane,
		/*
		* A short 3" or 6" straight section of can also be added at the center of the chicane. This would prolong 
		* the steady throttle section of the turn, making it even more challenging to drive.
		* [-1?] Striaght
		* [0] ~Same Angle 
		* [1] ~Same Angle Oppisite Direction Increasing Radius
		* [2?] Striaght
		*/
		ChicaneIncreasing,
		/*
		* A short 3" or 6" straight section of can also be added at the center of the chicane. This would prolong 
		* the steady throttle section of the turn, making it even more challenging to drive.
		* [-1?] Striaght
		* [0] ~Same Angle 
		* [1] ~Same Angle Oppisite Direction Decreasing Radius
		* [2?] Striaght
		*/
		ChicaneDecreasing,
		/*
		 *  Esses, like the chicane mentioned above require heavy braking prior to the entry. Esses work to slow 
		 * a car down much more though than a simple chicane. 
		 * 
		 * Esses, while often thought of as a single course element, are really two distinct turns placed end-to-end.
		 * 
		 * like the chicane, the entry and exit sections can be of different radius sizes to better facilitate a
		 * transition to whatever type of track section they proceed.
		 */
		EsseIn,
		/*
		 * [-1?] 
		 * [0-1] ~Same Angle 
		 * [2?] Striaght
		 */ 
		EsseOut,


		Washboard,

		Bustop = EsseIn | EsseOut | Chicane,
		/* 
		 * Banked turns allow a car to travel much faster than they normally would through a flat turn.
		 * Banked turns work best when placed before a long straight. This arrangement allows the cars 
		 * to exit the banked section at a higher rate of speed, and consequently travel down a long straight 
		 * faster than they would if they exited a flat turn.
		 */  
		Banked
	}
}

