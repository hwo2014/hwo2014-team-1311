﻿using System;

namespace teamgsn
{
	public class CarID {

		public static readonly string TEAM_GSN = "Team GSN";
		public static readonly string SLOW = "Slow";
		public static readonly string SIMPLE = "Simple";
		public static readonly string SPEED = "Speed";
		public static readonly string BLESSED = "Blessed";
		public static readonly string BRIAN = "Brian";
		public static readonly string JIM = "Jim";

		public string name;
		public string color;

		public CarID () {
		}

		//TODO:chbfiv should be isYourCar
		public bool isGsnCar {
			get {
				return name == TEAM_GSN;
			}
		}
	}
}

