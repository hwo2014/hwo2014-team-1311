using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class Msg {
		public string msgType;
		public Object data;

		public Msg(string msgType, Object data) {
			this.msgType = msgType;
			this.data = data;
		}
	}
}

