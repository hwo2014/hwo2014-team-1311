using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class JoinRace : ISerialize {
		public BotId botId;
		public string trackName;
		public int carCount;
		public string password;

		public JoinRace(string trackName, int carCount, string password = null, BotId botId = null) {
			this.botId = botId == null ? BotId.GSN_BOT_ID : botId;
			this.trackName = trackName;
			this.carCount = carCount;
			this.password = password;
		}

		public JoinRace(string name, string key, string trackName, int carCount, string password = null)
			: this(trackName, carCount, password, new BotId(name, key)) {

		}

		public string Serialize(int gameTick) {
			Msg msg = new Msg ("joinRace", this);
			return JsonHelper.Serialize(msg);
		}

		public static JoinRace JoinGermanyRace(int carCount = 1, string password = null) {
			return new JoinRace ("germany", carCount, password);
		}

		public static JoinRace JoinFinlandRace(int carCount = 1, string password = null) {
			return new JoinRace ("keimola", carCount, password);
		}
	}
}

