﻿using System;

namespace teamgsn
{
	public class TrackSequence
	{
		public int index = 0;
		public string hint = string.Empty;
		public float position = 0f;
		public float distance = 0f;
		public bool isTurn = false;
		public float risk = 0f;
		public int seqIndex = 0;
		public int lap = 0;
		public int pieceIndex = 0;

		public bool hasBest = false;
		public string bestCarName = CarID.TEAM_GSN;
		public float bestSpeed = 0f;
		public float bestAvgSpeed = 0;
		public float bestSlipAngleMax = 0;
		public int bestLane = 0;

		public TrackSequence (int index, string hint) {
			this.index = index;
			this.hint = hint;
		}

//		public TrackSequence (int index, string hint, float position = 0f, float distance = 0f, float speed = 0f, bool isTurn = false, bool isPast = false)
//		{
//			this.index = index;
//			this.hint = hint;
//			this.position = position;
//			this.distance = distance;
//			this.speed = speed;
//			this.isTurn = isTurn;
//			this.isPast = isPast;
//			this.isPast = isPast;
//		}
//
		public override string ToString ()
		{
			return string.Format ("[{0}:{1}:{2}]\tpos:{3:0.00}\tdist:{4:0.00}\tspeed:{5:0.00}\risk:{6:0.00}", 
				seqIndex, hint, isTurn, position, distance, bestSpeed, risk);
		}
	}
}

