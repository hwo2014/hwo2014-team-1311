using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public class CreateRace : ISerialize {
		public BotId botId;
		public string trackName;
		public int carCount;
		public string password;

		public CreateRace(string trackName, int carCount, string password = null, BotId botId = null) {
			this.botId = botId == null ? BotId.GSN_BOT_ID : botId;
			this.trackName = trackName;
			this.carCount = carCount;
			this.password = password;
		}

		public CreateRace(string name, string key, string trackName, int carCount, string password = null)
			: this(trackName, carCount, password, new BotId(name, key)) {

		}

		public string Serialize(int gameTick) {
			Msg msg = new Msg ("createRace", this);
			return JsonHelper.Serialize(msg);
		}

		public static CreateRace CreateGermanyRace(int carCount = 1, string password = null) {
			return new CreateRace ("germany", carCount, password);
		}

		public static CreateRace CreateFinlandRace(int carCount = 1, string password = null) {
			return new CreateRace ("keimola", carCount, password);
		}
	}
}

