﻿using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class Track {
		public string id;
		public string name;
		public List<TrackPiece> pieces = new List<TrackPiece>();
		public List<TrackLane> lanes = new List<TrackLane>();

		public static readonly string GERMANY = "germany";
		public static readonly string FINLAND = "keimola";
		public static readonly string EASTER_EGG = "usa";
		public static readonly string FRANCE = "france";

		public Track () {

		}

		public float getLength() {
			float length = 0;
			foreach (TrackPiece piece in pieces) {
				length += piece.length;
			}
			return length;
		}

		public SwitchLanes getSwitchLaneDirection (CarPositionContext cpc) {
			return getSwitchLaneDirection (cpc.pieceIndex, cpc.startLaneIndex, cpc.endLaneIndex);
		}

		public SwitchLanes getSwitchLaneDirection (int pieceIndex, int laneIndex, int nextLaneIndex) {
			SwitchLanes sl = SwitchLanes.None;

			int pieceIndexWrapped = (pieceIndex + 1) < pieces.Count ? pieceIndex : 0;
			TrackPiece nextPiece = pieces [pieceIndexWrapped];

			if (nextPiece.doesSwitch && laneIndex != nextLaneIndex) {
				float laneDistance = lanes [laneIndex].distanceFromCenter;
				float nextLaneDistance = lanes [nextLaneIndex].distanceFromCenter;

				sl = nextLaneDistance < laneDistance ? SwitchLanes.Left : SwitchLanes.Right;
			}

			return sl;
		}
	}
}

