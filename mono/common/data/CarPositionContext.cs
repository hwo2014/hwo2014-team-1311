using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace teamgsn
{
	public class CarPositionContext {
		public string name;
		public int lap;
		public int pieceIndex;
		public float inPiecePos;
		public float pieceDistance;
		public bool pieceSwitch;	
		public float pieceAngle;
		public float pieceRadius;
		public float pieceRadiusOffset;
		public int startLaneIndex;
		public int endLaneIndex;
		public float slipAngle;
		public CarPosition carPos;
		public List<CarPositionContext> shortest = new List<CarPositionContext>();
		public List<CarPositionContext> longest = new List<CarPositionContext>();

		public bool isTurn {
			get {
				return !RaceMath.fuzzyZero(pieceAngle);
			}
		}

		public bool isPast {
			get {
				return carPos != null;
			}
		}

		public CarPositionContext(string name, int lap, int pieceIndex, int startLaneIndex, int endLaneIndex) {
			this.name = name;
			this.lap = lap;
			this.pieceIndex = pieceIndex;
			this.startLaneIndex = startLaneIndex;
			this.endLaneIndex = endLaneIndex;
			this.inPiecePos = 0;
			this.slipAngle = 0;
		}

		public CarPositionContext(CarPosition cp) {
			this.name = cp.id.name;
			this.lap = cp.piecePosition.lap;
			this.pieceIndex = cp.piecePosition.pieceIndex;
			this.inPiecePos = cp.piecePosition.inPieceDistance;
			this.startLaneIndex = cp.piecePosition.lane.startLaneIndex;
			this.endLaneIndex = cp.piecePosition.lane.endLaneIndex;
			this.slipAngle = cp.angle;
			this.carPos = cp;
		}

		public void init(Race race) {
			TrackPiece tp = race.track.pieces [pieceIndex];

			pieceDistance = TrackService.measurePieceDistance (race.track.lanes, tp, startLaneIndex, endLaneIndex);
			pieceSwitch = tp.doesSwitch;	
			pieceAngle = tp.angle;
			pieceRadius = tp.radius;

			if (startLaneIndex == endLaneIndex) {
				TrackLane lane = race.track.lanes [startLaneIndex];
				pieceRadiusOffset = pieceRadius + (lane.distanceFromCenter * -1f);
			} else {
				TrackLane startLane = race.track.lanes [startLaneIndex];
				TrackLane endLane = race.track.lanes [endLaneIndex];
				pieceRadiusOffset = pieceRadius + ((startLane.distanceFromCenter + endLane.distanceFromCenter) * -1f);
			}
		}
	}
}

