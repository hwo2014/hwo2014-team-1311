using System;
using System.Collections.Generic;

namespace teamgsn
{
	public enum ExampleDriverStates {
		Religious,
		Slow,
		Fast,
		Demon
	}

	public class ExampleDriver : BotModule
	{
		// CONSTS
		public new static readonly string TAG = typeof (ExampleDriver).Name;

		// FIELDS
		private ExampleDriverStates m_state = ExampleDriverStates.Religious;
		private float m_throttle = 1.0f;
		private SwitchLanes m_switchLane = SwitchLanes.None;
		private float m_religiousSpeed = 0.5f;

		// CTOR
		public ExampleDriver (string trackName, int carCount, string password, float religiousSpeed)
			: base(trackName, carCount, password)
		{
			m_religiousSpeed = religiousSpeed;
		}

		// FLOW OVERRIDES
		public override SwitchLane getLane() {
			return new SwitchLane(m_switchLane);
		}

		public override Throttle getThrottle() {
			return new Throttle (m_throttle);
		}
			
		// LOGICAL OVERRIDES
		// NOTE: ton of other overrides available

		protected override void onGameInit (Race race)
		{
			base.onGameInit (race);
			Log.D (TAG, "Racing ExampleDriver ...");
		}

		protected override void onGameTick (int gameTick)
		{
			base.onGameTick (gameTick);

			m_state = pickState();

			switch (m_state) {
			case ExampleDriverStates.Religious:
				tickReligious ();
				break;
			case ExampleDriverStates.Slow:
				tickSlow ();
				break;
			case ExampleDriverStates.Fast:
				tickFast ();
				break;
			case ExampleDriverStates.Demon:
				tickDemon ();
				break;
			}
		}

		// STATE
		private ExampleDriverStates pickState() {
			// simple switch on this cars lap
			if (raceService.getLap(carName) == 0) 
				return ExampleDriverStates.Religious;
			else if (raceService.getLap(carName) == 1) 
				return ExampleDriverStates.Fast;
			else 
				return ExampleDriverStates.Demon;
		}

		private void tickReligious() {
			//const speed
			if (raceService.getSpeed () > m_religiousSpeed)
				m_throttle = 0f;
			else
				m_throttle = 1.0f;
		}

		private void tickSlow() {
			//slow throttle
			m_throttle = 0.2f;
		}

		private void tickFast() {
			//fast throttle
			m_throttle = 0.6f;
		}

		private void tickDemon() {
			//really fast throttle
			m_throttle = 0.7f;
			// always change lanes
			//TODO
		}
	}
}

