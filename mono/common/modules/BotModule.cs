using System;
using System.Collections.Generic;

namespace teamgsn
{
	public abstract class BotModule
	{
		public static readonly string TAG = typeof (BotModule).Name;
		
		public TimeService timeService { get { return Manager.instance.timeService; } }
		public RaceService raceService { get { return Manager.instance.raceService; } }
		public TrackService trackService { get { return Manager.instance.trackService; } }
		public HistoryService historyService { get { return Manager.instance.historyService; } }
		
		public string gameId { get { return timeService.gameId; } }
		public int gameTick { get { return timeService.gameTick; } }

		protected string carName;
		protected string trackName;
		protected int carCount;
		protected string password;

		protected static readonly int REPORT_TICK_MOD = 15;

		public BotModule(string trackName, int carCount, string password) {
			this.trackName = trackName;
			this.carCount = carCount;
			this.password = password;
		}

		public static BotModule CreateModule(string botName, string trackName, int carCount, string password) {
			BotModule mod;
			if (botName == CarID.TEAM_GSN) {
				mod = new SpeedDriver (trackName, carCount, password);
			} else if (botName == CarID.SLOW) {
				mod = new SpeedDriver (trackName, carCount, password);
			} else if (botName == CarID.SIMPLE) {
				mod = new SimpleDriver (trackName, carCount, password, 0.5f);
			} else if (botName == CarID.BLESSED) {
				mod = new BlessedDriver (trackName, carCount, password);
			} else if (botName == CarID.SPEED) {
				mod = new SpeedDriver (trackName, carCount, password);
			} else if (botName == CarID.BRIAN) {
				mod = new BrianDriver (trackName, carCount, password);
			} else if (botName == CarID.JIM) {
				mod = new JimDriver (trackName, carCount, password);
			} else {
				mod = new ExampleDriver (trackName, carCount, password, 6.0f);
			}
			return mod;
		}

		#region DO (Setup before ON)

		public void doJoinRace() {
			onJoinRace ();
		}

		public void doCarPositions (List<CarPosition> positions) {

			raceService.addCarPositions (gameTick, positions);

			onCarPositions (positions);

			onGameTick (gameTick);
		}
		
		public void doGameInit (Race race) {
			
			raceService.setGameInit (race);

			onGameInit (race);
		}

		public void doJoin() { onJoin (); }

		public void doGameStart() { onGameStart (); }

		public void doGameEnd() { onGameEnd (); }

		public void doSpawn() { onSpawn (); }

		public void doCrash() { onCrash (); }

		public void doLapFinished() { onLapFinished (); }

		public void doDisqualified() {
			onDisqualified ();
		}

		public void doTurboAvailable() { onTurboAvailable (); }

		public void doFinish() {
			onFinish ();
		}

		#endregion

		#region ON (Overrides for Modules)

		protected virtual void onGameTick(int gameTick) {
			if (Log.RAW_ONLY || (gameTick % REPORT_TICK_MOD) == 0) {

				Throttle throttle = getThrottle ();

				CarPosition cp = raceService.getCarById (carName);
				int lap = raceService.getLap(carName);
				float speed = raceService.getSpeed (carName);
				float avgSpeed = raceService.getSpeed (carName, gameTick, 0);
				float positon = raceService.getPosition (carName);
				float acceleration = raceService.getAcceleration (carName);
				float slip = raceService.getSlipAngle (carName);

				string msg = String.Format ("{0}\t{1:0.00}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7:0.00}\t{8:0.00}\t{9:0.00}", 
			             gameTick, throttle.value, speed, acceleration, slip, 
			             lap, cp.piecePosition.pieceIndex,
			             cp.piecePosition.inPieceDistance, positon, avgSpeed);

				if (Log.RAW_ONLY) Log.R (msg);
				else Log.D (TAG, msg);
			}
		}

		protected virtual void onJoinRace() {

		}

		protected virtual void onCarPositions (List<CarPosition> positionData) {}

		protected virtual void onGameInit (Race race) {
			Log.D (TAG, "\tLaps:" + race.raceSession.laps);
			Log.D (TAG, "\tLane\tDistance\tTotalDistance");
			foreach (TrackLane lane in race.track.lanes) {
				float distance = raceService.getLapDistance (lane.index);
				Log.D (TAG, String.Format ("\t{0}\t{1:0.00}\t\t{2:0.00}", lane.index, distance, distance * race.raceSession.laps));
			}

			string title = "Tik\tThr\tSpd\tAcc\tSlp\tLap\tPc\tInPc\tPos\tAvgSpd";
			if (Log.RAW_ONLY) Log.R (title);
			else Log.D (TAG, title);
		}
		
		protected virtual void onJoin () {}

		protected virtual void onGameStart () {}

		protected virtual void onGameEnd () {}

		protected virtual void onSpawn () {}

		protected virtual void onCrash () {}

		protected virtual void onLapFinished () {}

		protected virtual void onDisqualified() { }

		protected virtual void onTurboAvailable() {}

		protected virtual void onFinish() { }

		#endregion

		#region CONTROL

		public abstract SwitchLane getLane ();
		
		public abstract Throttle getThrottle ();

		public virtual Turbo getTurbo () {
			return new Turbo ();
		}

		#endregion

		public void setCarName(string name) {
			carName = name;
		}

	}
}

