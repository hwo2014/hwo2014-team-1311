using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class SpeedDriver : BotModule
	{
		public new static readonly string TAG = typeof (SpeedDriver).Name;

//		private float m_targetSpeed = 1.0f;
		private float m_throttle = 1f;
//		private float m_factor = 1f;
//		private float m_speed = 10f;
		private float ACCE_UPPER_LIMIT = 0.01f;
		private float ACCE_LOWER_LIMIT = -0.01f;
		private float SPEED_MAX = 1.0f;

		public SpeedDriver (string trackName, int carCount, string password)
			: base(trackName, carCount, password)
		{
//			m_targetSpeed = targetSpeed;
		}

		protected override void onGameTick (int gameTick)
		{
			CarPositionContext cpc = raceService.getCarContextById (carName);

//			CarPosition cp = raceService.getCarById (carName);

			int lap = cpc.lap;
			int lane = cpc.endLaneIndex;
			//			float speed = raceService.getSpeed (carName);
			float avgSpeed = raceService.getSpeed (carName, gameTick, 0);
			float positon = raceService.getPosition (carName);
			float slip = cpc.slipAngle;

			float pos = raceService.getPosition (carName);
			float speed = 0f;
			float acceleration = 0f;

			for (int i = 0; i < raceService.race.cars.Count; i++) {
				string localCarName = raceService.race.cars[i].id.name;

				float localSpeed = raceService.getSpeed (carName);
				float localAccel = raceService.getAcceleration (carName);

				if (localCarName == carName) {
					speed = localSpeed;
					acceleration = localAccel;
				}

				if (localAccel > ACCE_UPPER_LIMIT)
					ACCE_UPPER_LIMIT = localAccel;

				if (localAccel < ACCE_LOWER_LIMIT)
					ACCE_LOWER_LIMIT = localAccel;

				if (localSpeed > SPEED_MAX)
					SPEED_MAX = localSpeed;
			}

			List<TrackSequence> track = trackService.getTrack (carName);

			TrackSequence ts = trackService.getNextTrackSeq (track, pos);

//			List<float> risks = trackService.getRiskSMV (track);

//			int riskIndex = (int)Math.Floor (pos);
//			float risk = risks [riskIndex];
//
//			if (float.IsNaN (risk)) {
//				Log.D (TAG, "ERROR");
//			}

			float slipRatio = 1 - (Math.Abs(cpc.slipAngle) / 60f);
			float slipAccel = raceService.getSlipAcceleration(carName);


//			float weighted = ((1f *risk) + (10f*slipRatio)) / (1f + 2f);
//			m_throttle = risk < slipRatio ? risk : slipRatio;
//			m_throttle = m_throttle * m_factor;

			if (ts.hasBest) {

				m_throttle = 1f;

				float distRemaining = ts.position - pos;

				float laneFactor = raceService.getLaneFactor (ts.pieceIndex, ts.bestLane, lane);
				float bestSpeed = ts.bestSpeed * laneFactor * slipRatio;
				//				float targetSpeed = SPEED_MAX;

				float ticksRemaining = speed > 0 ? distRemaining / speed : distRemaining;

				float projectedAccel = (bestSpeed - speed) / ticksRemaining;

				if (!ts.isTurn && projectedAccel <= ACCE_LOWER_LIMIT) {
					m_throttle = 1f * laneFactor * ts.risk;
				} else if (projectedAccel <= ACCE_LOWER_LIMIT) {
					m_throttle = 0f;
				}

//				if (Math.Abs(slipAccel) > 0.25f)
//					m_throttle = 0f;
			} else {
				m_throttle = 0.5f;
			}

			string msg = String.Format ("{0}\t{1:0.00}\t{2:0.00}\t{3:0.00}\t{4:0.00}\t{5}\t{6}\t{7:0.00}\t{8:0.00}\t{9:0.00}\t{10:0.00}\t{11:0.00}\t{12:0.00}\t{13:0.00}\t{14:0.00}\t{15}\t{16}", 
				gameTick, m_throttle, speed, acceleration, slip, 
				lap, cpc.pieceIndex,
				cpc.inPiecePos, positon, avgSpeed, ts.distance, ts.position, ts.risk, slipRatio, slipAccel, ts.hint, ts.isTurn);

			if (Log.RAW_ONLY) 
				Log.R (msg);
			else if ((gameTick % REPORT_TICK_MOD) == 0)
				Log.D (TAG, msg);

		}

		protected override void onGameInit (Race race)
		{
			base.onGameInit (race);
			Log.D (TAG, "Racing SpeedDriver ...");
		}
	
		protected override void onLapFinished ()
		{
//			string msg = String.Format ("({0}) Speed:{1:0.00}, AvgSpeed:{2:0.00}, Position:{3:0.00}", 
//				timeService.gameTick, raceService.getSpeed (), raceService.getAvgSpeed (), raceService.getPosition ()); 
//			Log.D (TAG, msg);
		}

		private int lastReportedPieceIndex = -1;

		public override SwitchLane getLane() {

			CarPositionContext cpc = raceService.getCarContextById (carName);

			if (lastReportedPieceIndex != cpc.pieceIndex)
				lastReportedPieceIndex = -1;

			SwitchLanes sl = trackService.getSwitchLane (carName);

			if (sl != SwitchLanes.None && lastReportedPieceIndex != cpc.pieceIndex) {
				lastReportedPieceIndex = cpc.pieceIndex;
				return new SwitchLane (sl);
			} else {
				return new SwitchLane (SwitchLanes.None);
			}

			//TODO:chbfiv add history checks to decide if we should still send

		}

		public override Throttle getThrottle() {
			return new Throttle (m_throttle);
		}
	}
}

