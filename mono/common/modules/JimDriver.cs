using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class JimDriver : BotModule
	{
		public new static readonly string TAG = typeof (JimDriver).Name;

		protected List<TrackPiece> trackPieces = new List<TrackPiece>();
		protected int currPiece;
		protected int currLap;
		protected int turbosAvailable;
		protected Race localRace;

		public JimDriver (string trackName, int carCount, string password)
			: base(trackName, carCount, password)
		{
			turbosAvailable = 0;
		}

		// update loop
		protected override void onCarPositions(List<CarPosition> positionData) {

			//DO SOMETHING
			for (int i = 0; i < positionData.Count; i++) {

				CarPosition cp = positionData[i];
				if (cp.id.isGsnCar) {
					// this is us.  All I want for right now is the piece index
					if (currPiece != cp.piecePosition.pieceIndex)
						Log.D (TAG, "JimDriver piece changed from " + currPiece + " to " + cp.piecePosition.pieceIndex + " next Turn: " + piecesUntilTurn());
					currPiece = cp.piecePosition.pieceIndex;
					currLap = cp.piecePosition.lap;
				}
			}
		}

		protected override void onGameInit (Race race)
		{
			base.onGameInit (race);
			localRace = race;
			List<TrackPiece> pieces = race.track.pieces;
				
			for (int i = 0; i < pieces.Count; i++) {
				trackPieces.Add (pieces[i]);
				Log.D (TAG, "JimDriver add " + JsonHelper.Serialize(pieces [i]));
			}
		}

		public override Throttle getThrottle() {
//			if ((getNextPiece (currPiece).angle == 0) && (getNextPiece (currPiece+1).angle == 0) && (getNextPiece (currPiece+2).angle == 0)) {
//				return new Throttle (0.7f);
//			} else {
//				return new Throttle (0.655f);
//			}

			int straightsAhead = piecesUntilTurn ();
			float targetThrottle = 1.0f;
//			Log.D(TAG, "GET THROTTLE straights ahead: " + straightsAhead + " " + trackPieces.Count);
			if (trackName == Track.FINLAND || trackName == Track.GERMANY || trackName == Track.FRANCE) {
				switch (straightsAhead) {
				case(0):
				case(1):
					targetThrottle = 0.655f;
					break;
				case(2):
					targetThrottle = 0.68f;
					break;
				case(3):
					targetThrottle = 0.805f;
					break;
				}
			} else if (trackName == Track.EASTER_EGG) {
				switch (straightsAhead) {
				case(1):
					targetThrottle = 0.94f;
					break;
				case(2):
					targetThrottle = 0.97f;
					break;
				case(3):
					targetThrottle = 1.0f;
					break;
				}
			}
			return new Throttle (targetThrottle);

//			if (checkForTurns (3)) {
//				return new Throttle (0.655f);
//			}
//			return new Throttle (0.805f);
		}

		public override SwitchLane getLane() {
//			return new SwitchLane (trackService.getSwitchLane (carName, gameTick));
			return new SwitchLane(SwitchLanes.None);
		}

		public override Turbo getTurbo ()
		{
			float rem = trackService.remainingLaneDistance (carName, gameTick);
			CarPosition cp = raceService.getCarById (carName, gameTick);
			float slip = (cp != null ? cp.angle : 72);
			int numTurnsLeft = numTurnsRemainingThisLap ();
			if (turbosAvailable > 0 && localRace.raceSession.laps == currLap+1 && numTurnsLeft < 1 && Math.Abs(slip) < 25) {
				Log.D (TAG, "using turbo? " + turbosAvailable);
				turbosAvailable--;
				return new Turbo (TurboOptions.Fire);
			}
			return new Turbo ();
		}

		protected override void onTurboAvailable ()
		{
			base.onTurboAvailable ();
			turbosAvailable++;
			Log.D (TAG, "onTurboAvailable " + turbosAvailable);
		}

		protected override void onCrash ()
		{
			base.onCrash ();
			Log.D(TAG, "Crash: " + getCurrTrackIndex());
		}

		public int getCurrTrackIndex() {
			return currPiece;
		}

		protected TrackPiece getNextPiece(int currPieceIndex) {
			if (currPieceIndex + 1 < trackPieces.Count) {
				return trackPieces [currPieceIndex + 1];
			} else if (trackPieces.Count > 0) {
				return trackPieces [(currPieceIndex % trackPieces.Count)];
			}
			return new TrackPiece (100, false, 15);
		}

		protected bool checkForTurns(int numPiecesToCheck) {
			for (int i = 1; i <= numPiecesToCheck; i++) {
				if (getNextPiece (currPiece + i).angle != 0) {
					return true;
				}
			}
			return false;
		}

		protected int piecesUntilTurn() {
			int i;
			for (i = 0; i <= 10 && i < trackPieces.Count; i++) {
				if (getNextPiece (currPiece + i).angle != 0) {
//					Log.D (TAG, "piecesUntilTurn " + i + " " + getNextPiece (currPiece + i).angle);
					break;
				}
			}
			return i;
		}

		protected int numTurnsRemainingThisLap() {
			int i;
			int turns = 0;
			for (i = trackPieces.Count - 1; i > currPiece; i--) {
				if (trackPieces[i].angle != 0) {
					turns++;
				}
			}
			return turns;
		}
	}
}

