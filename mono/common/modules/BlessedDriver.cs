using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class BlessedDriver : BotModule
	{
		// CONSTS
		public new static readonly string TAG = typeof (BlessedDriver).Name;

		// FIELDS
		private float m_throttle = 1.0f;
		private SwitchLanes m_switchLane = SwitchLanes.None;

		// CTOR
		public BlessedDriver (string trackName, int carCount, string password)
			: base(trackName, carCount, password)
		{

		}

		// FLOW OVERRIDES
		public override SwitchLane getLane() {
			return new SwitchLane(m_switchLane);
		}

		public override Throttle getThrottle() {
			return new Throttle (m_throttle);
		}
			
		// LOGICAL OVERRIDES
		// NOTE: ton of other overrides available

		protected override void onGameInit (Race race)
		{
			base.onGameInit (race);
			Log.D (TAG, "Racing BlessedDriver ...");
		}

		protected override void onGameTick (int gameTick)
		{
			base.onGameTick (gameTick);

			if (trackName == Track.FINLAND) {
				tickFinland ();
			} else if (trackName == Track.GERMANY) {
				tickDefault ();
			} else {
				tickDefault ();
			}
		}

		// STATE
		#region FINLAND

		private void tickFinland() {

			float carPos = raceService.getPosition (carName);
//			float lapDistance = raceService.getLapDistance (carName);
//			int lap = raceService.getLap (carName);
//
//			carPos = carPos - (lap * lapDistance);

			// simple switch on this cars lap
			// hardcoded
			if (carPos >= 0 && carPos < (440f)) {
				m_throttle = 1f;
			} else if (carPos >= (440f) && carPos < 560f) { //lap1turn1 
				m_throttle = 0f; // 82	7.97	4.87	399.32	0.00	0.00
			} else if (carPos >= (560f) && carPos < 1240f) { 
				m_throttle = 1f; // 104	5.94	5.38	559.02	57.64	57.64
			} else if (carPos >= (1240f) && carPos < 1470f) { //lap1turn2
				m_throttle = 0f; // 191	9.26	6.62	1263.90	-11.06	-11.06
			} else if (carPos >= (1470f) && carPos < 1750f) { 
				m_throttle = 1f; // 214	5.82	6.77	1448.32	-55.44	-55.44
			} else if (carPos >= (1750f) && carPos < 1930f) { //lap1turn3
				m_throttle = 0f; // 256	8.12	7.00	1792.61	-13.70	-13.70
			} else if (carPos >= (1930f) && carPos < 2320f) {
				m_throttle = 1f; // 270	6.12	7.00	1890.68	56.57	56.57
			} else if (carPos >= (2320f) && carPos < 2590f) { //lap1turn4
				m_throttle = 0f; // 331	8.87	7.17	2373.94	-14.94	-14.94
			} else if (carPos >= (2590f) && carPos < 2860f) { 
				m_throttle = 1f; // 357	5.25	7.15	2551.52	50.94	50.94
			} else if (carPos >= (2860f) && carPos < 2940f) { //lap1turn5
				m_throttle = 0f; // 398	7.92	7.19	2861.72	-23.57	-23.57
			} else if (carPos >= (2940f) && carPos < 3960f) {
				m_throttle = 1f; // 409	6.34	7.19	2939.08	41.69	41.69
			} else if (carPos >= (3960f) && carPos < 4140f) { //lap2turn1
				m_throttle = 0f; // 528	9.65	7.49	3957.04	-0.17	-0.17
			} else if (carPos >= (4140f) && carPos < 4870f) {
				m_throttle = 1f; // 552	6.07	7.50	4142.59	53.62	53.62
			} else if (carPos >= (4870f) && carPos < 5080f) { //lap2turn2
				m_throttle = 0f; // 640	9.34	7.61	4870.37	-11.76	-11.76
			} else if (carPos >= (5080f) && carPos < 5400f) {
				m_throttle = 1f; // 666	5.52	7.62	5072.99	-59.90	-59.90
			} else if (carPos >= (5400f) && carPos < 5480f) { //lap2turn3
				m_throttle = 0f; // 707	7.95	7.64	5400.87	-10.07	-10.07
			} else if (carPos >= (5480f) && carPos < 5680f) {
				m_throttle = 1f; // 718	6.37	7.63	5478.54	42.67	42.67
			} else if (carPos >= (5680f) && carPos < 5700f) { //lap2turn4
				m_throttle = 0f; // 746	7.82	7.61	5677.37	53.12	53.12
			} else if (carPos >= (5700f) && carPos < 5980f) {
				m_throttle = 1f; // 749	7.55	7.61	5700.49	56.82	56.82
			} else if (carPos >= (5980f) && carPos < 6150f) { //lap2turn5
				m_throttle = 0f; // 782	8.74	7.65	5980.03	-13.23	-13.23
			} else if (carPos >= (6150f) && carPos < 6470f) {
				m_throttle = 1f; // 807	5.28	7.62	6149.93	47.47	47.47
			} else if (carPos >= (6470f) && carPos < 6550f) { //lap2turn6
				m_throttle = 0f; // 850	7.93	7.61	6471.20	-23.14	-23.14
			} else if (carPos >= (6550f) && carPos < 25000f) {
				m_throttle = 1f; // 861	6.35	7.61	6548.66	42.53	42.53
			} else {
				m_throttle = 1f;
			}

			m_throttle = m_throttle * 0.9f;
		}

		#endregion

		private void tickDefault() {
			m_throttle = 0.5f;
		}
	}
}

