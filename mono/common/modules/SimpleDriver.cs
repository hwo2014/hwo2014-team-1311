using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class SimpleDriver : BotModule
	{
		// CONSTS
		public new static readonly string TAG = typeof (SimpleDriver).Name;

		// FIELDS
		private float m_throttle = 1.0f;

		// CTOR
		public SimpleDriver (string trackName, int carCount, string password, float throttle)
			: base(trackName, carCount, password)
		{
			m_throttle = throttle;
		}

		// FLOW OVERRIDES
		public override SwitchLane getLane() {
			return new SwitchLane(SwitchLanes.None);
		}

		public override Throttle getThrottle() {
			return new Throttle (m_throttle);
		}
			
		// LOGICAL OVERRIDES
		// NOTE: ton of other overrides available

		protected override void onGameInit (Race race)
		{
			base.onGameInit (race);
			Log.D (TAG, "Racing SimpleDriver ...");
		}

		protected override void onGameTick (int gameTick)
		{
			float distanceToNextTurn = trackService.getShortestDistanceToNextTurn (carName);
			if (distanceToNextTurn < 200f) {
				m_throttle = 0.5f;
			} else {
				m_throttle = 1.0f;
			}

			base.onGameTick (gameTick);
		}
	}
}

