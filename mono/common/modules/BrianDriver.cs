using System;
using System.Collections.Generic;

namespace teamgsn
{
	public class BrianDriver : BotModule
	{
		// CONSTS
		public new static readonly string TAG = typeof (BrianDriver).Name;
		public static readonly float MIN_THROTTLE = 0.18f;
		public static readonly float MAX_THROTTLE = 0.84f;
		public static readonly float MAX_SLIP = 20.0f;
		public static readonly float MAX_SLIP_INCREASE = 1.0f;

		// FIELDS
		private float m_throttle = 1.0f;
		private float oldSlipAngle = 0.0f;
		private SwitchLanes m_switchLane = SwitchLanes.None;

		// CTOR
		public BrianDriver (string trackName, int carCount, string password)
			: base(trackName, carCount, password)
		{

		}

		// FLOW OVERRIDES
		public override SwitchLane getLane() {
			return new SwitchLane(m_switchLane);
		}

		public override Throttle getThrottle() {
			return new Throttle (m_throttle);
		}
			
		// LOGICAL OVERRIDES
		// NOTE: ton of other overrides available

		protected override void onGameInit (Race race)
		{
			Log.D (TAG, "Racing BrianDriver ...");
		}

		protected override void onGameTick (int gameTick)
		{
			base.onGameTick (gameTick);
			if (gameTick == 20) {
				m_switchLane = SwitchLanes.None;
			} else {
				//m_switchLane = SwitchLanes.Right; //Taking the inner route actually takes longer because it can't corner as fast
			}
			float slipAngle = Math.Abs(raceService.getSlipAngle (carName));
			if (slipAngle > oldSlipAngle + MAX_SLIP_INCREASE) {
				m_throttle = 0.0f;
				Log.D (TAG, "Slip Angle: " + slipAngle + " -- Increasing Too Fast, Cutting Throttle");
			} else if (slipAngle >= MAX_SLIP) {
				m_throttle = MIN_THROTTLE;
				Log.D (TAG, "Slip Angle: " + slipAngle + " -- Cutting Throttle");
			} else if (slipAngle < oldSlipAngle - 0.1f) {
				m_throttle = 1.0f;
				Log.D (TAG, "Slip Angle: " + slipAngle + " -- Straightening Out, Max Throttle");
			} else {
				m_throttle = MAX_THROTTLE - ((MAX_THROTTLE - MIN_THROTTLE) * slipAngle / MAX_SLIP);
				if (slipAngle > 0) {
					Log.D (TAG, "Slip Angle: " + slipAngle + " -- Reducing Throttle to " + m_throttle);
				}
			}
			oldSlipAngle = slipAngle;
		}
			

	}
}

