using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public static class JsonHelper {
		public static readonly string TAG = typeof (JsonHelper).Name;

		public static string Serialize(object obj) {
			// empty object?
			string ret = "{}";
			try {
				ret = JsonConvert.SerializeObject(obj);
			} catch (Exception ex) {
				Log.E (TAG, ex.Message);
			}
			return ret;
		}
	}
}

