using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public static class RaceMath {
		public static readonly string TAG = typeof (RaceMath).Name;

//		public static float scale(float val, float factor) {
//			return 
//		}

		public static bool fuzzyTest(float x, float y) {
			float lowerLimit = (y - float.Epsilon);
			float upperLimit = (y + float.Epsilon);
			return lowerLimit <= x && upperLimit >= x;
		}
			
		public static bool fuzzyZero(double x) {
			return Math.Abs (x) <= double.Epsilon;
		}

		public static bool fuzzyZero(float x) {
			return Math.Abs (x) <= float.Epsilon;
		}
	}
}

