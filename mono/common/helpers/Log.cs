using System;
using Newtonsoft.Json;

namespace teamgsn
{
	public static class Log {

		public static readonly bool RAW_ONLY = false;

		public static void D(string tag, string message) {
			LogInternal ("debug", tag, message);
		}

		public static void E(string tag, string message) {
			LogInternal ("error", tag, message);
		}
		
		public static void I(string tag, string message) {
			LogInternal ("info", tag, message);
		}

		public static void R(string message) {
			Console.WriteLine (message);
		}
		
		private static void LogInternal(string type, string tag, string message) {
			if (!RAW_ONLY) Console.WriteLine ("[" + type + "][" + tag + "] " + message);
		}
	}
}

