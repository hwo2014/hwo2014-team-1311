﻿using System;

namespace teamgsn
{
	public class Manager
	{
		private static Manager m_instance;
		private static readonly object m_lock = new object();

		public RaceService raceService = new RaceService();
		public TimeService timeService = new TimeService();
		public TrackService trackService = new TrackService();
		public HistoryService historyService = new HistoryService();

		static Manager ()
		{
		}

		public static Manager instance {
			get {
				lock (m_lock) {
					if (m_instance == null)
						m_instance = new Manager ();
				}

				return m_instance;
			}
		}
	}
}

