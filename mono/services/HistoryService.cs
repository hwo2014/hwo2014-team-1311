﻿using System;
using System.Collections.Generic;

namespace teamgsn
{

	public class HistoryService
	{
		public TimeService timeService { get { return Manager.instance.timeService; } }
		public RaceService raceService { get { return Manager.instance.raceService; } }
		public TrackService trackService { get { return Manager.instance.trackService; } }
		public HistoryService historyService { get { return Manager.instance.historyService; } }

		private SwitchLanes m_pendingSwitch = SwitchLanes.None;
		private int m_pendingSwitchPiece = -1;

		public HistoryService ()
		{

		}
//			
//		public bool canSwitch(int gameTick) {
//			TrackPiece p = null;
//
//			return p;
//		}
//
//		public bool hasPendingSwitch(int gameTick) {
//			TrackPiece p = null;
//
//			return p;
//		}

		public void setPendingSwitchStatus(SwitchLanes pendingSwitch, int gameTick) {
			m_pendingSwitch = pendingSwitch;
		}
	}
}

