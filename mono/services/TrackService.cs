﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace teamgsn
{
	public class TrackService
	{
		public static readonly string TAG = typeof (TrackService).Name;

		public TimeService timeService { get { return Manager.instance.timeService; } }
		public RaceService raceService { get { return Manager.instance.raceService; } }
		public HistoryService historyService { get { return Manager.instance.historyService; } }

		private float[,] m_shortest;
		private DijkstraFast m_dijkstra;
		private Race m_race;

		public TrackService()
		{

		}

		private int getVertexIndex(string carId) {
			return getVertexIndex (carId, timeService.gameTick);
		}

		private int getVertexIndex(string carId, int gameTick) {

			int lapIndex = raceService.getLap (carId, gameTick);
			int laneIndex = raceService.getLane (carId, gameTick);
			int pieceIndex = raceService.getPiece (carId, gameTick);

			return getVertexIndex (lapIndex, laneIndex, pieceIndex);
		}

		private int getVertexIndex(int lap, int lane, int piece) {
			int laneCount = m_race.track.lanes.Count;
			int piecesCount = m_race.track.pieces.Count;

			return piece + (lane * piecesCount) + (lap * piecesCount * laneCount);

			// 3 laps, 2 lanes, 40 pieces

			// 0 + (0 * 40) + (0 * 40 * 2) = 0
			// 1 + (0 * 40) + (0 * 40 * 2) = 1
			// ...
			// 0 + (1 * 40) + (0 * 40 * 2) = 40
			// 1 + (1 * 40) + (0 * 40 * 2) = 41
			// ...
			// 0 + (0 * 40) + (1 * 40 * 2) = 80
			// 1 + (0 * 40) + (1 * 40 * 2) = 81
		}

		private int getLap(int index) {
			int laneCount = m_race.track.lanes.Count;
			int piecesCount = m_race.track.pieces.Count;
			float f = index / (float)(piecesCount * laneCount);
			return (int)Math.Floor (f);
		}	

		private int getLane(int index) {
			int laneCount = m_race.track.lanes.Count;
			int piecesCount = m_race.track.pieces.Count;
			float f = (index % (laneCount * piecesCount)) / (float)piecesCount;
			return (int)Math.Floor (f);
		}		

		private int getPiece(int index) {
			int laneCount = m_race.track.lanes.Count;
			int piecesCount = m_race.track.pieces.Count;
			return (index % (laneCount * piecesCount)) % piecesCount;
		}

		public void init(Race race) {
			m_race = race;

			// populate
			int lapCount = race.raceSession.laps;
			int laneCount = race.track.lanes.Count;
			int pieceCount = race.track.pieces.Count;
			int vertexCount = lapCount * laneCount * pieceCount;

			m_shortest = new float[vertexCount, vertexCount];

			// need default values to be -1 for edge
			for (int i = 0; i < vertexCount; i++) {
				for (int j = 0; j < vertexCount; j++) {
					m_shortest [i, j] = -1f;
				}
			}

			//TODO:chbfiv Add Cap Edges for finish (not start)

			// Lap(s)
			for (int lapIndex = 0; lapIndex < lapCount; lapIndex++) {
				bool isLastLap = lapIndex == lapCount - 1;

				// Piece(s)
				for (int pieceIndex = 0; pieceIndex < pieceCount; pieceIndex++) {
					bool isLastPiece = pieceIndex == pieceCount - 1;
					TrackPiece piece = race.track.pieces [pieceIndex];
					// last piece is connected to first
					int nextPieceIndex = isLastPiece ? 0 : pieceIndex + 1;
					int nextLapIndex = isLastPiece && !isLastLap ? lapIndex + 1 : lapIndex;

					// Lane(s)
					for (int laneIndex = 0; laneIndex < laneCount; laneIndex++) {
						bool isLastLane = laneIndex == laneCount - 1;

						if (isLastLap && isLastLane && isLastPiece)
							break;

						int vertexIndex = getVertexIndex (lapIndex, laneIndex, pieceIndex);

						int edgeCount;
						int edgeIndexDefault;
						if (piece.doesSwitch) {
							// at least 2 lanes
							if (laneIndex == 0) { // outside lane
								edgeIndexDefault = laneIndex;
								edgeCount = edgeIndexDefault + 2;
							} else if (laneIndex == (laneCount - 1)) { //inside lane
								edgeIndexDefault = laneIndex - 1;
								edgeCount = edgeIndexDefault + 2;
							} else  {
								edgeIndexDefault = laneIndex - 1;
								edgeCount = edgeIndexDefault + 3;
							}
						} else {
							edgeIndexDefault = laneIndex;
							edgeCount = edgeIndexDefault + 1;
						}

						for (int edgeIndex = edgeIndexDefault; edgeIndex < edgeCount; edgeIndex++) {
						
							int nextVertexIndex = getVertexIndex (nextLapIndex, edgeIndex, nextPieceIndex);

							float weight = measurePieceDistance (race.track.lanes, piece, laneIndex, edgeIndex);

							m_shortest [vertexIndex, nextVertexIndex] = weight;
							//m_matrix [nextVertexIndex, vertexIndex] = weight;
						} // end edgeIndex
					} // end laneIndex
				} // end pieceIndex
			} // end laneIndex

//			Log.D (TAG, this.ToString());

			m_dijkstra = new DijkstraFast(vertexCount,
				new DijkstraFast.InternodeTraversalCost(GetInternodeTraversalCost),
				new DijkstraFast.NearbyNodesHint(GetNearbyNodesHint));

			// start lane and piece
			int[] path = m_dijkstra.GetMinimumPath (0, vertexCount - 1);

			string solution = "\n";
			string solution2 = "\n";
			float solutionDistance = 0f;
			float solutionDistance2 = 0f;
			for (int i =0; i < path.Length - 1; i++) {
				int id = path[i];
				int nextId = path[i+1];
				float weight = m_shortest[id,nextId];

				if (weight > 0) {
					solution += string.Format("\t{0}>{1}:{2:0.00}", id, nextId, weight);
					solution2 += id + ">";
					solutionDistance += weight;
					solutionDistance2 += weight;

					if (i % 5 == 0)
						solution += "\n";

					if ((i + 1) % race.track.pieces.Count == 0) {
						solution2 += string.Format (" ({0:0.00})\n", solutionDistance2);
						solutionDistance2 = 0f;
					}
				}
			}
			string solutionHeader = string.Format("Shortest Track is {0:0.00}", solutionDistance);
			Log.D (TAG, solutionHeader);
//			Log.D (TAG, solution2);
//			Log.D (TAG, solution);
		}

		public static float measurePieceDistance(List<TrackLane> lanes, TrackPiece piece, int startLaneIndex, int endLaneIndex) {

			float distance = 0f;

			TrackLane startLane = lanes[startLaneIndex];
			TrackLane endLane = lanes[endLaneIndex];

			float laneOffset = startLane.distanceFromCenter * -1f;

			if (!RaceMath.fuzzyZero (piece.angle)) { 
				if (startLaneIndex != endLaneIndex) {
					double c1 = piece.radius + startLane.distanceFromCenter;
					double c2 = piece.radius + endLane.distanceFromCenter;
					double c = c1 > c2 ? c1 : c2; 
					double a = c1 > c2 ? c2 : c1; 
					double b = Math.Sqrt (Math.Pow (c, 2) - Math.Pow (a, 2));
					distance = (float)b;
//					Log.D(TAG, string.Format("C[{0}>{1}]:{2:0.00}", startLaneIndex, endLaneIndex, distance));
				} else {
					float laneOffsetInverted = laneOffset * Math.Sign (piece.angle);
					distance = ((float)Math.PI * Math.Abs (piece.angle) / 180) * (piece.radius + laneOffsetInverted);
				}
			} else {
				if (startLaneIndex != endLaneIndex) {
					double a = Math.Abs (startLane.distanceFromCenter) + Math.Abs (endLane.distanceFromCenter);
//					double b1 = piece.length * 0.44f;
//					double b2 = piece.length * 0.66f;
					double b = piece.length;
					double c = Math.Sqrt (Math.Pow (a, 2) + Math.Pow (b, 2));
//					distance = (float)c + (float)b1;
					distance = (float)c;
//					Log.D(TAG, string.Format("F[{0}>{1}]:{2:0.00}", startLaneIndex, endLaneIndex, distance));
				} else {
					distance = piece.length;
				}
			}


			return distance;
		}

		public SwitchLanes getSwitchLane() {
			return getSwitchLane(CarID.TEAM_GSN, timeService.gameTick);
		}

		public SwitchLanes getSwitchLane(int gameTick) {
			return getSwitchLane(CarID.TEAM_GSN, gameTick);
		}

		public SwitchLanes getSwitchLane(string carId) {
			return getSwitchLane(carId, timeService.gameTick);
		}

		public SwitchLanes getSwitchLane(string carId, int gameTick) {
			// never try to switch lanes on first two ticks
			if (gameTick < 15) {
				return SwitchLanes.None;
			}

			CarPositionContext cpc = raceService.getCarContextById (carId, gameTick);

			SwitchLanes sl = SwitchLanes.None;

			if (cpc.shortest.Count > 0) {
				sl = m_race.track.getSwitchLaneDirection (cpc.shortest[0]);
			}

			return sl;
		}

		public void calculatePaths(CarPositionContext cpc) {

			// shortest path
			if (cpc.lap < 0)
				return;

			int startIndex = getVertexIndex (cpc.lap, cpc.startLaneIndex, cpc.pieceIndex);
			int endIndex = m_shortest.GetLength (0) - 1;
			int[] shortestPath = m_dijkstra.GetMinimumPath (startIndex, endIndex);

			for(int i = 0; i < shortestPath.Length - 1; i++) {
				int index = shortestPath [i];
				int nextIndex = shortestPath [i+1];

				int lapIndex = getLap(index);
				int pieceIndex = getPiece(index);
				int startLaneIndex = getLane(index);
				int endLaneIndex = getLane(nextIndex);

				CarPositionContext futureCpc = new CarPositionContext(cpc.name, lapIndex, pieceIndex, startLaneIndex, endLaneIndex);
				//TODO:chbfiv confirm
				//float distance = m_shortest [index, nextIndex];
				futureCpc.init (m_race);

				cpc.shortest.Add (futureCpc);
			}

			// longest path

		}

		// get costs. If there is no connection, then cost is maximum.
		private float GetInternodeTraversalCost(int start, int finish)
		{
			float cost = m_shortest [start, finish];
			if (cost < 0) 
				return float.MaxValue;
			else
				return cost;
		}

		private IEnumerable<int> GetNearbyNodesHint(int startingNode)
		{
			int totalNodes = m_shortest.GetLength (0);
			List<int> nearbyNodes = new List<int>(totalNodes);

			for (int i = 0; i < totalNodes; i++) {

				if (i == startingNode)
					continue;

				float cost = m_shortest[startingNode, i];

				if (cost > 0)
					nearbyNodes.Add (i);
			}
			return nearbyNodes;
		}

		public int getRemainingTurnPieceCount() {
			return getRemainingTurnPieceCount(CarID.TEAM_GSN, timeService.gameTick);
		}

		public int getRemainingTurnPieceCount(int gameTick) {
			return getRemainingTurnPieceCount(CarID.TEAM_GSN, gameTick);
		}

		public int getRemainingTurnPieceCount(string carId) {
			return getRemainingTurnPieceCount(carId, timeService.gameTick);
		}

		public int getRemainingTurnPieceCount(string carId, int gameTick) {

			int count = 0;

			CarPosition cp = raceService.getCarById (carId, gameTick); 

			if (cp != null) {
				for (int i = m_race.track.pieces.Count - 1; i >= cp.piecePosition.pieceIndex; i--) {
					TrackPiece piece = m_race.track.pieces [i];
					if (piece != null && !RaceMath.fuzzyZero (piece.angle)) {
						count++;
					}
				}
			}

			return count;
		}

		public float remainingLaneDistance(string carId, int gameTick) {
			CarPosition cp = raceService.getCarById (carId, gameTick);
			if (cp != null) {
				float d = raceService.getPosition (carId, gameTick);
				float totalRaceDistance = raceService.getLapDistance (cp.piecePosition.lane.endLaneIndex) * m_race.raceSession.laps;
				float remainingDistance = totalRaceDistance - d;
				return remainingDistance;
			}
			return -1;
		}

		public float getShortestDistanceToNextTurn() {
			return getShortestDistanceToNextTurn(CarID.TEAM_GSN, timeService.gameTick);
		}

		public float getShortestDistanceToNextTurn(int gameTick) {
			return getShortestDistanceToNextTurn(CarID.TEAM_GSN, gameTick);
		}

		public float getShortestDistanceToNextTurn(string carId) {
			return getShortestDistanceToNextTurn(carId, timeService.gameTick);
		}

		public float getShortestDistanceToNextTurn(string carId, int gameTick) {
			CarPositionContext cpc = raceService.getCarContextById (carId, gameTick);
			return getShortestDistanceToNextTurn (cpc); 
		}

		public float getShortestDistanceToNextTurn(CarPositionContext cpc) {

			int index = getVertexIndex (cpc.lap, cpc.startLaneIndex, cpc.pieceIndex);
			int[] path = m_dijkstra.GetMinimumPath (index, m_shortest.GetLength(0) - 1);

			float distance = 0;

			for (int i = 0; i < path.Length - 1; i++) {
				int tpieceIndex = getPiece (path[i]);
				int npieceIndex = getPiece (path[i+1]);

				TrackPiece piece = m_race.track.pieces [tpieceIndex];
				if (!RaceMath.fuzzyZero (piece.angle)) {
						return distance;
				} else {
					distance += m_shortest[tpieceIndex,npieceIndex];
				}
			}

			return distance;
		}

		public float getShortestDistanceToNextStraight() {
			return getShortestDistanceToNextStraight(CarID.TEAM_GSN, timeService.gameTick);
		}

		public float getShortestDistanceToNextStraight(int gameTick) {
			return getShortestDistanceToNextStraight(CarID.TEAM_GSN, gameTick);
		}

		public float getShortestDistanceToNextStraight(string carId) {
			return getShortestDistanceToNextStraight(carId, timeService.gameTick);
		}

		public float getShortestDistanceToNextStraight(string carId, int gameTick) {
			CarPositionContext cpc = raceService.getCarContextById (carId, gameTick);
			return getShortestDistanceToNextStraight (cpc); 
		}

		public float getShortestDistanceToNextStraight(CarPositionContext cpc) {

			int index = getVertexIndex (cpc.lap, cpc.startLaneIndex, cpc.pieceIndex);
			int[] path = m_dijkstra.GetMinimumPath (index, m_shortest.GetLength(0) - 1);

			float distance = 0;

			for (int i = 0; i < path.Length - 1; i++) {
				int tpieceIndex = getPiece (path[i]);
				int npieceIndex = getPiece (path[i+1]);

				TrackPiece piece = m_race.track.pieces [tpieceIndex];
				if (RaceMath.fuzzyZero (piece.angle)) {
					return distance;
				} else {
					distance += m_shortest[tpieceIndex,npieceIndex];
				}
			}

			return distance;
		}

		public bool shouldSliceSequence(CarPositionContext from, CarPositionContext to) {
			return !((from.isTurn && to.isTurn) ||
				(!from.isTurn && !to.isTurn));
		}

		public void sliceSequence(List<TrackSequence> track, CarPositionContext from, CarPositionContext to,
			float entryPos, float exitPos,
			int turnPieces = 0, float turnRadius = 0f, float turnAngle = 0f) {

			//TODO:chbfiv Break apart inverted turns

			if (from.isTurn) {

				if (turnPieces == 0) {
					Log.D (TAG, "ERROR");
				}

				//TODO:chbfiv dynamic apex? late apex?
				float apexDist = (exitPos - entryPos) * 0.5f; //50%
				float exitDist = (exitPos - entryPos) * 0.5f; //50%

				float risk1 = (turnRadius / turnPieces) / ((turnRadius / turnPieces) + Math.Abs(turnAngle));
				float risk2apex = apexDist / (apexDist + Math.Abs(turnAngle)); // Apex Risk
				float risk2exit = exitDist / (exitDist + Math.Abs(turnAngle)); // Exit Risk

				//Apex
				TrackSequence tsApex = new TrackSequence (track.Count, "TURN APEX");
				tsApex.isTurn = from.isTurn;
				tsApex.lap = to.lap;
				tsApex.pieceIndex = from.pieceIndex + (int)Math.Round((float)(to.pieceIndex - from.pieceIndex) / 2f);
				tsApex.seqIndex = from.pieceIndex * 100000;
				tsApex.distance = apexDist;
				tsApex.position = entryPos + apexDist;
				tsApex.bestSpeed = 0f;
				tsApex.risk = risk1 < risk2apex ?  risk1 : risk2apex;
//				tsApex.risk = ((1f * tsApex.risk) + (9f * 0.01f)) / 10f; // weighted
//				tsApex.risk = 0f;
				track.Add (tsApex);
//			
//				if (float.IsNaN (tsApex.risk)) {
//					Log.D (TAG, "ERROR");
//				}

				//Exit
				TrackSequence tsExit = new TrackSequence (track.Count, "STRA ENTRY");
				tsExit.isTurn = to.isTurn;
				tsExit.lap = to.lap;
				tsExit.pieceIndex = to.pieceIndex;
				tsExit.seqIndex = to.pieceIndex * 10000;
				tsExit.distance = exitDist;
				tsExit.position = exitPos;
				tsExit.bestSpeed = 0f;
				tsExit.risk = risk1 < risk2exit ?  risk1 : risk2exit;
				tsExit.risk = ((1f * tsExit.risk) + (9f * 1f)) / 10f; // weighted
//				tsExit.risk = 1f;
				track.Add (tsExit);

//				if (float.IsNaN (tsExit.risk)) {
//					Log.D (TAG, "ERROR");
//				}
			} else {
				//Entry
				TrackSequence ts = new TrackSequence (track.Count, "TURN ENTRY");
				ts.isTurn = to.isTurn;
				ts.lap = to.lap;
				ts.pieceIndex = to.pieceIndex;
				ts.seqIndex = to.pieceIndex * 1000;
				ts.distance = (exitPos - entryPos);
				ts.position = exitPos;
				ts.bestSpeed = 0f;
				ts.risk = 1f;
				track.Add (ts);
//
//				if (float.IsNaN (ts.risk)) {
//					Log.D (TAG, "ERROR");
//				}
			}
		}

		public float getTopSpeed(List<TrackSequence> track) {
			float topSpeed = 10f; //defualt

			if (track.Count <= 1)
				return topSpeed;

//			int gameTick = timeService.gameTick;

			TrackSequence from = track [0];
			for (int i = 1; i < track.Count - 1; i++) {
				TrackSequence to = track [i];

				if (from.isTurn && to.isTurn) { // apex

				} else if (from.isTurn && !to.isTurn) { // turn exit
					//to = straight
				} else if (!from.isTurn && to.isTurn) { // turn entry
					//from = straight
				} else { //!from.isTurn && !to.isTurn //straight
					//from & to = straight
				}
			}

			return topSpeed;
		}

		public int getFutureTrackSeqIndex(List<TrackSequence> track, float currentPos) {
			for (int i = 0; i < track.Count; i++) {
				TrackSequence ts = track [i];
				if (ts.position >= currentPos)
					return i;
			}

			return 0;
		}

		public TrackSequence getNextTrackSeq(List<TrackSequence> track, float currentPos) {

			TrackSequence next = track [getFutureTrackSeqIndex (track, currentPos)];

			UpdateSpeedsOLD (track, next);

			return next;
		}

		//Simple Moving Average
		public List<float> getRiskSMV(List<TrackSequence> track) {

			List<float> risks = new List<float> ();

			for (int i = 0; i < track.Count; i++) {
				TrackSequence ts = track [i];
				int dist = (int)Math.Floor (ts.distance);

				for (int j = 0; j < dist; j++) {
					risks.Add (ts.risk);
				}
			}

			int riskCount = risks.Count;

			// always odd
			int sampleCount = (int)Math.Floor((float)riskCount / (float)track.Count); 
			sampleCount = sampleCount % 2 == 0 ? sampleCount + 1 : sampleCount;

			int wingSampleCount = ((sampleCount - 1) / 2);

			List<float> rawRisks = new List<float> (risks);

			for (int i = 0; i < riskCount; i++) {
				int lowerSampleIndex = i - wingSampleCount;
				lowerSampleIndex = Math.Max (0, lowerSampleIndex);

				int upperSampleIndex = i + wingSampleCount;
				upperSampleIndex = Math.Min (riskCount - 1, upperSampleIndex);

				float weight = 0f;
				int count = 0;
				for (int j = lowerSampleIndex; j <= upperSampleIndex; j++) {
					weight += risks[j];
					count++;
				}

				weight = count > 0 ? weight / (float)count : weight;

				rawRisks [i] = weight;
			}

			int bufferCount = 100;
			for (int i = 0; i < bufferCount; i++) {
				rawRisks.Add (1f);
			}

			return rawRisks;
		}

		public List<float> getRisks(List<TrackSequence> track) {

			List<float> risks = new List<float> ();

			for (int i = 0; i < track.Count; i++) {
				TrackSequence ts = track [i];
				int dist = (int)Math.Floor (ts.distance);

				for (int j = 0; j < dist; j++) {
					risks.Add (ts.risk);
				}
			}

			int bufferCount = 100;
			for (int i = 0; i < bufferCount; i++) {
				risks.Add (1f);
			}

			return risks;
		}

//		public List<TrackSequence> getFutureTrackByFactor(List<TrackSequence> track, float factor) {
//
//			List<TrackSequence> limitedTrack = new List<TrackSequence>();
//
//			float seqDistance = 0f;
//
//			for (int i = 0; i < track.Count; i++) {
//				TrackSequence ts = track [i];
//
//				if (!ts.isPast) {
//					seqDistance = Math.Max (seqDistance, ts.distance);
//				}
//			}
//
//			for (int i = 0; i < track.Count; i++) {
//				TrackSequence ts = track [i];
//
//				if (!ts.isPast && ts.distance >= (seqDistance*factor)) {
//					limitedTrack.Add (ts);
//				}
//			}
//
//			return limitedTrack;
//		}

		public List<TrackSequence> getTrack(string carName) {

			List<TrackSequence> track = new List<TrackSequence> ();

			CarPositionContext from = raceService.getCarContextById (carName, 0);

			float entryPos = 0f;
			float exitPos = 0f;

			// Initial Track Sequence
			TrackSequence initalTs = new TrackSequence (0, "INITAL");
			initalTs.isTurn = from.isTurn;
			initalTs.distance = 0;
			initalTs.position = 0;
			initalTs.bestSpeed = 12f;
			initalTs.risk = 1f;
			track.Add (initalTs);

			int turnPieces = 0;
			float turnRadius = 0f;
			float turnAngle = 0f;

			// PAST
			for (int i = 1; i <= timeService.gameTick; i++) {
				CarPositionContext to = raceService.getCarContextById (carName, i);

				if (from.pieceIndex != to.pieceIndex) {
					exitPos += from.pieceDistance;

					if (from.isTurn) {
						turnPieces++;
						turnRadius += from.pieceRadiusOffset;
						turnAngle += Math.Abs(from.pieceAngle);
					}
				}

				if (shouldSliceSequence (from, to)) {
					sliceSequence (track, from, to, entryPos, exitPos, turnPieces, turnRadius, turnAngle);
					entryPos = exitPos;

					turnPieces = 0;
					turnRadius = 0f;
					turnAngle = 0f;
				}

				from = to;
			}

			// FUTURE

			//Use Shortest for now
			List<CarPositionContext> carPosList = new List<CarPositionContext> (from.shortest);

			for (int i = 0; i < carPosList.Count; i++) {
				CarPositionContext to = carPosList[i];

				exitPos += from.pieceDistance;

				if (from.isTurn) {
					turnPieces++;
					turnRadius += from.pieceRadiusOffset;
					turnAngle += Math.Abs(from.pieceAngle);
				}

				if (shouldSliceSequence (from, to)) {
					sliceSequence (track, from, to, entryPos, exitPos, turnPieces, turnRadius, turnAngle);
					entryPos = exitPos;

					turnPieces = 0;
					turnRadius = 0f;
					turnAngle = 0f;
				}

				from = to;
			}

//			ClassifyTrack (track);

//			UpdateLapIndex (track);
//			UpdateSpeeds (track);

			return track;
		}


		public void UpdateLapIndex(List<TrackSequence> track) {

//			if (track.Count <= 1)
//				return;
//
//			int laps = m_race.raceSession.laps;
//
//			TrackSequence from = track [0];
//			for (int i = 1; i < track.Count - 1; i++) {
//				TrackSequence to = track [i];
//
//				if (from.lap != to.lap) {
//					lapDist = totalDist;
//				} 
//
//				totalDist += to.distance;
//
//				float seqPos = to.position - lapDist;
//				to.seqIndex = (int)Math.Round(seqPos);
//			}
		}


		public void UpdateSpeedsOLD(List<TrackSequence> track, TrackSequence next) {
			//TODO:chbfiv get GameTick per Position per CarName

			//over the range from -> to, whom had the best average speed?
			//update from.speed w/ that entry speed
			//this could be from any car
			//weed out crashed cars?

			//update speed
			//worst slip for that speed

			if (track.Count <= 1)
				return;

			string bestCarName = CarID.TEAM_GSN;
			float bestAvgSpeed = 0f;
			int bestGameTickA = 0;
			int bestGameTickB = 0;
			bool hasBest = false;

			TrackSequence from = track [0];
			for (int i = 0; i < track.Count - 1; i++) {
				TrackSequence to = track [i];

				if (next.seqIndex != to.seqIndex) {
					from = to;
					continue;
				}

				for (int j = 0; j < m_race.cars.Count; j++) {
					string carName = m_race.cars [j].id.name;

					int gameTickA = raceService.getGameTick (carName, from.position);
					int gameTickB = raceService.getGameTick (carName, to.position);

					if (gameTickA != -1 && gameTickB != -1) {
						float avgSpeed = raceService.getSpeed (carName, gameTickA, gameTickB);

						float slipAcc = raceService.getSlipAcceleration (carName, gameTickB, gameTickB - 5);

						if (slipAcc < 1f && avgSpeed > bestAvgSpeed) {
							bestCarName = carName;
							bestGameTickA = gameTickA;
							bestGameTickB = gameTickB;
						}
						hasBest = true;
					}
				}

				from = to;
			}

			if (hasBest) {
				next.hasBest = true;
				next.bestCarName = bestCarName;
				next.bestLane = raceService.getLane (bestCarName, bestGameTickB);
				next.bestSpeed = raceService.getSpeed (bestCarName, bestGameTickB);
				next.bestAvgSpeed = bestAvgSpeed;
				next.bestSlipAngleMax = raceService.getSlipAngleMax (bestCarName, bestGameTickA, bestGameTickB);
			}
		}

		public void UpdateSpeedsOLD(List<TrackSequence> track) {
		
			//TODO:chbfiv get GameTick per Position per CarName

			//over the range from -> to, whom had the best average speed?
			//update from.speed w/ that entry speed
			//this could be from any car
			//weed out crashed cars?

			//update speed
			//worst slip for that speed

			if (track.Count <= 1)
				return;

			TrackSequence from = track [0];

			for (int i = 0; i < track.Count - 1; i++) {
				TrackSequence to = track [i];

				string bestCarName = CarID.TEAM_GSN;
				float bestAvgSpeed = 0f;
				int bestGameTickA = 0;
				int bestGameTickB = 0;
				bool hasBest = false;

				TrackSequence fromfrom = track [0];
				for (int j = 0; j < track.Count - 1; j++) {
					TrackSequence toto = track [i];

					if (from.seqIndex != fromfrom.seqIndex)
						continue;

					for (int k = 0; k < m_race.cars.Count; k++) {
						string carName = m_race.cars [k].id.name;

						int gameTickA = raceService.getGameTick (carName, fromfrom.position);
						int gameTickB = raceService.getGameTick (carName, toto.position);

						if (gameTickA != -1 && gameTickB != -1) {
							float avgSpeed = raceService.getSpeed (carName, gameTickA, gameTickB);

							if (avgSpeed > bestAvgSpeed) {
								bestCarName = carName;
								bestGameTickA = gameTickA;
								bestGameTickB = gameTickB;
							}
							hasBest = true;
						}
					}
				}

				if (hasBest) {
					from.hasBest = true;
					from.bestCarName = bestCarName;
					from.bestSpeed = raceService.getSpeed (bestCarName, bestGameTickB);
					from.bestAvgSpeed = bestAvgSpeed;
					from.bestSlipAngleMax = raceService.getSlipAngleMax (bestCarName, bestGameTickA, bestGameTickB);
				}
			}

			//what about to?
		}

		public void ClassifyTrack(List<TrackSequence> track) {

			if (track.Count <= 1)
				return;

			TrackSequence from = track [0];
			float deltaDist = 0f;

			for (int i = 1; i < track.Count - 1; i++) {
				TrackSequence to = track [i];

				to.distance += deltaDist * -1f;

				if (from.isTurn && to.isTurn) { // apex
					//extend by 20 %
					deltaDist = to.distance * 0.2f;
					to.distance += deltaDist;
					to.position += deltaDist;
				} else if (from.isTurn && !to.isTurn) { // turn exit
					//shorten by 50%
					deltaDist = -1f * to.distance * 0.5f;
					to.distance += deltaDist;
					to.position += deltaDist;
				} else if (!from.isTurn && to.isTurn) { // turn entry
					//shorten by 20%
					deltaDist = -1f * to.distance * 0.2f;
					to.distance += deltaDist;
					to.position += deltaDist;
				} else { //!from.isTurn && !to.isTurn //straight
					//no change
					deltaDist = 0f;
				}
			}
		}

		public override string ToString() {

			string ret = string.Empty;

			for (int i = 0; i < m_shortest.GetLength (0); i++) {
				for (int j = 0; j < m_shortest.GetLength (1); j++) {
					ret += string.Format("{0:0.00}\t", m_shortest[i,j]);
				}
				ret += "\n";
			}

			return ret;
		}
	}
}

