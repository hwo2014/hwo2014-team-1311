using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace teamgsn
{
	public class RaceService
	{
		public static readonly string TAG = typeof (RaceService).Name;

		public TimeService timeService { get { return Manager.instance.timeService; } }
		public TrackService trackService { get { return Manager.instance.trackService; } }
		public HistoryService historyService { get { return Manager.instance.historyService; } }

		private Dictionary<int, List<CarPositionContext>> m_carPosHistory = new Dictionary<int, List<CarPositionContext>>();
		public Race race;

		public int DELTA_TICKS = 5;
		public bool SHOULD_CALC_PATHS = true;

		public RaceService ()
		{
		}

		#region Data

		public void addCarPositions (int gameTick, List<CarPosition> positions) {
			if (!m_carPosHistory.ContainsKey (gameTick)) {
				List<CarPositionContext> cpcs = new List<CarPositionContext> ();
				for (int i=0; i < positions.Count; i++) {
					CarPosition cp = positions[i];
					CarPositionContext cpc = new CarPositionContext (cp);
					cpc.init (race);
					if (SHOULD_CALC_PATHS) trackService.calculatePaths (cpc);
					cpcs.Add (cpc);
				}

				m_carPosHistory [gameTick] = cpcs;
			} else {
				Log.E (TAG, "car positions already exist for this tick " + gameTick);
			}
		}

		public void setGameInit (Race race) {
			this.race = race;
			trackService.init (race);
		}

		#endregion

		#region Shortest Path


		#endregion

		#region Slip Angle

		/// <summary>
		/// Gets the slip angle for "Your Car" for the currentTick (timeService.gameTick)
		/// </summary>
		/// <returns>The slip angle.</returns>
		public float getSlipAngle() {
			return getSlipAngle(CarID.TEAM_GSN, timeService.gameTick, timeService.gameTick);
		}

		/// <summary>
		/// Gets the slip angle for "Your Car" for gameTick
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="gameTick">Game tick.</param>
		public float getSlipAngle(int gameTick) {
			return getSlipAngle(CarID.TEAM_GSN, gameTick, gameTick);
		}

		/// <summary>
		/// Gets the average slip angle for "Your Car" for gameTickA through gameTickB
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="gameTickA">Game tick a.</param>
		/// <param name="gameTickB">Game tick b.</param>
		public float getSlipAngle(int gameTickA, int gameTickB) {
			return getSlipAngle(CarID.TEAM_GSN, gameTickA, gameTickB);
		}

		/// <summary>
		/// Gets the slip angle for carId for the currentTick (timeService.gameTick)
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="carId">Car identifier.</param>
		public float getSlipAngle(string carId) {
			return getSlipAngle(carId, timeService.gameTick, timeService.gameTick);
		}

		/// <summary>
		/// Gets the average slip angle for carId for gameTickA through gameTickB
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="carId">Car identifier.</param>
		/// <param name="gameTickA">Game tick a.</param>
		/// <param name="gameTickB">Game tick b.</param>
		public float getSlipAngle(string carId, int gameTickA, int gameTickB) {
			float angle = 0;

			//clamp
			gameTickA = Math.Min (gameTickA, timeService.gameTick);
			gameTickA = Math.Max (gameTickA, 0);
			gameTickB = Math.Min (gameTickB, timeService.gameTick);
			gameTickB = Math.Max (gameTickB, 0);

			int initialTick = gameTickA > gameTickB ? gameTickB : gameTickA;
			int finalTick = gameTickA > gameTickB ? gameTickA : gameTickB;
			int deltaTicks = finalTick - initialTick;

			for (int i = initialTick; i <= finalTick; i++) {
				CarPosition cp = getCarById (carId, i);
				if (cp != null) {
					angle += cp.angle;
				}
			}
				
			float avg = angle / (float)(deltaTicks + 1);
			return avg;
		}

		/// <summary>
		/// Gets the slip angle max for "Your Car" for the all gameTicks
		/// </summary>
		/// <returns>The slip angle.</returns>
		public float getSlipAngleMax() {
			return getSlipAngleMax(CarID.TEAM_GSN, 0, timeService.gameTick);
		}

		/// <summary>
		/// Gets the slip angle max for "Your Car" for the all gameTicks
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="gameTick">Game tick.</param>
		public float getSlipAngleMax(int gameTick) {
			return getSlipAngleMax(CarID.TEAM_GSN, 0, gameTick);
		}

		/// <summary>
		/// Gets the slip angle max for "Your Car" for gameTickA through gameTickB
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="gameTickA">Game tick a.</param>
		/// <param name="gameTickB">Game tick b.</param>
		public float getSlipAngleMax(int gameTickA, int gameTickB) {
			return getSlipAngleMax(CarID.TEAM_GSN, gameTickA, gameTickB);
		}

		/// <summary>
		/// Gets the slip angle max for carId for the currentTick (timeService.gameTick)
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="carId">Car identifier.</param>
		public float getSlipAngleMax(string carId) {
			return getSlipAngleMax(carId, timeService.gameTick, timeService.gameTick);
		}

		/// <summary>
		/// Gets the slip angle max for carId for gameTickA through gameTickB
		/// </summary>
		/// <returns>The slip angle.</returns>
		/// <param name="carId">Car identifier.</param>
		/// <param name="gameTickA">Game tick a.</param>
		/// <param name="gameTickB">Game tick b.</param>
		public float getSlipAngleMax(string carId, int gameTickA, int gameTickB) {
			float angle = 0;

			//clamp
			gameTickA = Math.Min (gameTickA, timeService.gameTick);
			gameTickA = Math.Max (gameTickA, 0);
			gameTickB = Math.Min (gameTickB, timeService.gameTick);
			gameTickB = Math.Max (gameTickB, 0);

			int initialGameTick = gameTickA < gameTickB ? gameTickA : gameTickB;
			int finalGameTick = gameTickA < gameTickB ? gameTickB : gameTickA;

			for (int i = initialGameTick; i <= finalGameTick; i++) {
				CarPosition cp = getCarById (carId, gameTickA);
				if (cp != null) {
					angle = Math.Max (Math.Abs(angle), Math.Abs(cp.angle));
				}
			}

			return angle;
		}

		public float getSlipAcceleration() {
			return getSlipAcceleration(CarID.TEAM_GSN, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getSlipAcceleration(int gameTickA) {
			return getSlipAcceleration(CarID.TEAM_GSN, gameTickA, gameTickA - DELTA_TICKS);
		}

		public float getSlipAcceleration(int gameTickA, int gameTickB) {
			return getSlipAcceleration(CarID.TEAM_GSN, gameTickA, gameTickB);
		}

		public float getSlipAcceleration(string carId) {
			return getSlipAcceleration(carId, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getSlipAcceleration(string carId, int gameTickA, int gameTickB) {
			//same because 1 tick is our unit of time ATM

			//clamp
			gameTickA = Math.Min (gameTickA, timeService.gameTick);
			gameTickA = Math.Max (gameTickA, 0);
			gameTickB = Math.Min (gameTickB, timeService.gameTick);
			gameTickB = Math.Max (gameTickB, 0);

			int initialTick = gameTickA > gameTickB ? gameTickB : gameTickA;
			int finalTick = gameTickA > gameTickB ? gameTickA : gameTickB;
			int deltaTick = finalTick - initialTick;

			float initialSlip = getSlipAngle(carId, initialTick, initialTick - DELTA_TICKS);
			float finalSlip = getSlipAngle(carId, finalTick, finalTick - DELTA_TICKS);
			float deltaSlip = finalSlip - initialSlip;

			float accel = deltaTick > 0 ? deltaSlip / deltaTick : 0;
			return accel;
		}

		#endregion

		#region Piece

		public int getPiece() {
			return getPiece(CarID.TEAM_GSN, timeService.gameTick);
		}

		public int getPiece(int gameTick) {
			return getPiece(CarID.TEAM_GSN, gameTick);
		}

		public int getPiece(string carId) {
			return getPiece(carId, timeService.gameTick);
		}

		public int getPiece(string carId, int gameTick) {
			int piece = 0;
			CarPosition cp = getCarById (carId, gameTick);

			if (cp != null) {
				piece = cp.piecePosition.pieceIndex;
			}

			return piece;
		}

		#endregion

		#region Lane

		public float getLaneFactor(int pieceIndex, int laneAIndex, int laneBIndex) {

			int laneCount = race.track.lanes.Count;

			if (laneAIndex == laneBIndex)
				return 1f;

			if (laneAIndex < 0 || laneAIndex >= laneCount)
				return 1f;

			if (laneBIndex < 0 || laneBIndex >= laneCount)
				return 1f;
				
			float laneADistCenter = race.track.lanes [laneAIndex].distanceFromCenter;
			float laneBDistCenter = race.track.lanes [laneBIndex].distanceFromCenter;

			// find piece

			int pieceCount = race.track.pieces.Count;

			if (pieceIndex < 0 || pieceIndex >= pieceCount)
				return 1f;

			TrackPiece piece = race.track.pieces [pieceIndex];

			float laneADist = Math.Abs(piece.radius + (laneADistCenter * -1f));
			float laneBDist = Math.Abs(piece.radius + (laneBDistCenter * -1f));

			float factor = laneADist > laneBDist ? laneBDist / laneADist : laneADist / laneBDist;

			return factor < 1f ? factor * 0.75f : factor;
		}

		public int getLane() {
			return getLane(CarID.TEAM_GSN, timeService.gameTick);
		}

		public int getLane(int gameTick) {
			return getLane(CarID.TEAM_GSN, gameTick);
		}

		public int getLane(string carId) {
			return getLane(carId, timeService.gameTick);
		}

		public int getLane(string carId, int gameTick) {
			int lane = 0;
			CarPosition cp = getCarById (carId, gameTick);

			if (cp != null) {
				lane = cp.piecePosition.lane.startLaneIndex;
			}

			return lane;
		}

		#endregion

		#region Lap

		public int getLap() {
			return getLap(CarID.TEAM_GSN, timeService.gameTick);
		}

		public int getLap(int gameTick) {
			return getLap(CarID.TEAM_GSN, gameTick);
		}

		public int getLap(string carId) {
			return getLap(carId, timeService.gameTick);
		}

		public int getLap(string carId, int gameTick) {
			int lap = 0;
			CarPosition cp = getCarById (carId, gameTick);

			if (cp != null) {
				lap = cp.piecePosition.lap;
			}

			return lap;
		}
			
		public float getLapDistance(int laneIndex) {

			float distance = 0;

			for (int j = 0; j < race.track.pieces.Count; j++) {
				TrackPiece tp = race.track.pieces[j];
				distance += TrackService.measurePieceDistance (race.track.lanes, tp, laneIndex, laneIndex);
			}

			return distance;
		}

		#endregion

		#region Distance
		
		public float getPosition() {
			return getPosition(CarID.TEAM_GSN, timeService.gameTick);
		}

		public float getPosition(int gameTick) {
			return getPosition(CarID.TEAM_GSN, gameTick);
		}

		public float getPosition(string carId) {
			return getPosition(carId, timeService.gameTick);
		}

		public float getPosition(string carId, int gameTick) {

			float pos = 0;

			CarPosition targetCp = getCarById (carId, gameTick);
			if (targetCp == null) {
				return pos;
			}

			//clamp
			gameTick = Math.Min (gameTick, timeService.gameTick);
			gameTick = Math.Max (gameTick, 0);

			for (int i = 0; i <= gameTick;) {
				CarPosition cp = getCarById (carId, i);

				if (targetCp.piecePosition.pieceIndex == cp.piecePosition.pieceIndex &&
					targetCp.piecePosition.lap == cp.piecePosition.lap) {
					pos += targetCp.piecePosition.inPieceDistance;
					break;
				} 

				int lap = cp.piecePosition.lap;
				int pieceIndex = cp.piecePosition.pieceIndex;
				int startLaneIndex = cp.piecePosition.lane.startLaneIndex;
				int endLaneIndex = cp.piecePosition.lane.endLaneIndex;

				TrackPiece piece = race.track.pieces [pieceIndex];
				pos += TrackService.measurePieceDistance (race.track.lanes, piece, startLaneIndex, endLaneIndex);

				while (i <= gameTick && pieceIndex == cp.piecePosition.pieceIndex && lap == cp.piecePosition.lap) {
					cp = getCarById (carId, i);
					i++;
				} 

				i--;
			}

			return pos;
		}

		#endregion

		#region GameTick

		public int getGameTick(string carId, float pos) {
		
			int gameTick = -1;
			pos = Math.Max (pos, 0f);

			for (int i = 0; i <= timeService.gameTick; i++) {
				float distance = getPosition (carId, i);

				if (distance > pos) {
					gameTick = i - 1;
					break;
				}
			}

			if (gameTick != -1) {
				//clamp
				gameTick = Math.Min (gameTick, timeService.gameTick);
				gameTick = Math.Max (gameTick, 0);
			}

			return gameTick;
		}

		#endregion

		#region Speed

//		public float getAvgSpeed() {
//			return getAvgSpeed(CarID.TEAM_GSN, timeService.gameTick);
//		}
//		
//		public float getAvgSpeed(int gameTick) {
//			return getAvgSpeed(CarID.TEAM_GSN, gameTick);
//		}
//
//		public float getAvgSpeed(string carId) {
//			return getAvgSpeed(carId, timeService.gameTick);
//		}
//
//		public float getAvgSpeed(string carId, int gameTick) {
//			gameTick = Math.Min (gameTick, timeService.gameTick);
//			gameTick = Math.Max (gameTick, 0);
//
//			float distance = 0;
//
//			distance = getPosition (carId, gameTick);
//
//			return gameTick > 0 ? distance / gameTick : distance;
//		}
			
		public float getSpeed() {
			return getSpeed(CarID.TEAM_GSN, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getSpeed(int gameTickA) {
			return getSpeed(CarID.TEAM_GSN, gameTickA, gameTickA - DELTA_TICKS);
		}

		public float getSpeed(int gameTickA, int gameTickB) {
			return getSpeed(CarID.TEAM_GSN, gameTickA, gameTickB);
		}

		public float getSpeed(string carId) {
			return getSpeed(carId, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getSpeed(string carId, int gameTickA) {
			return getSpeed(carId, gameTickA, gameTickA - DELTA_TICKS);
		}

		public float getSpeed(string carId, int gameTickA, int gameTickB) {
			//clamp
			gameTickA = Math.Min (gameTickA, timeService.gameTick);
			gameTickA = Math.Max (gameTickA, 0);
			gameTickB = Math.Min (gameTickB, timeService.gameTick);
			gameTickB = Math.Max (gameTickB, 0);

			float pos1 = getPosition(carId, gameTickA);
			float pos2 = getPosition (carId, gameTickB);

			float deltaPos = pos1 > pos2 ? pos1 - pos2 : pos2 - pos1;
			int deltaTicks = gameTickA > gameTickB ? gameTickA - gameTickB : gameTickB - gameTickA;

			float speed = deltaTicks > 0 ? deltaPos / deltaTicks : deltaPos;
			return speed;
		}

		public float getAcceleration() {
			return getAcceleration(CarID.TEAM_GSN, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getAcceleration(int gameTickA) {
			return getAcceleration(CarID.TEAM_GSN, gameTickA, gameTickA - DELTA_TICKS);
		}

		public float getAcceleration(int gameTickA, int gameTickB) {
			return getAcceleration(CarID.TEAM_GSN, gameTickA, gameTickB);
		}

		public float getAcceleration(string carId) {
			return getAcceleration(carId, timeService.gameTick, timeService.gameTick - DELTA_TICKS);
		}

		public float getAcceleration(string carId, int gameTickA, int gameTickB) {
			//same because 1 tick is our unit of time ATM

			//clamp
			gameTickA = Math.Min (gameTickA, timeService.gameTick);
			gameTickA = Math.Max (gameTickA, 0);
			gameTickB = Math.Min (gameTickB, timeService.gameTick);
			gameTickB = Math.Max (gameTickB, 0);

			int initialTick = gameTickA > gameTickB ? gameTickB : gameTickA;
			int finalTick = gameTickA > gameTickB ? gameTickA : gameTickB;
			int deltaTick = finalTick - initialTick;

			float initialSpeed = getSpeed(carId, initialTick, initialTick - DELTA_TICKS);
			float finalSpeed = getSpeed(carId, finalTick, finalTick - DELTA_TICKS);
			float deltaSpeed = finalSpeed - initialSpeed;

			float accel = deltaTick > 0 ? deltaSpeed / deltaTick : 0;
			return accel;
		}

		#endregion

		#region Car

		public CarPosition getCarById() {
			return getCarById(CarID.TEAM_GSN);
		}
		
		public CarPosition getCarById(int gameTick) {
			return getCarById(CarID.TEAM_GSN, gameTick);
		}

		public CarPosition getCarById(string carId) {
			return getCarById (carId, timeService.gameTick);
		}

		public CarPosition getCarById(string carId, int gameTick) {

			if (m_carPosHistory.ContainsKey(gameTick)) {
				List<CarPositionContext> cpcs = m_carPosHistory [gameTick];
				foreach (CarPositionContext cpc in cpcs) {
					if (cpc.name == carId)
						return cpc.carPos;
				}
			}  

			return null;
		}

		public CarPositionContext getCarContextById() {
			return getCarContextById(CarID.TEAM_GSN);
		}

		public CarPositionContext getCarContextById(int gameTick) {
			return getCarContextById(CarID.TEAM_GSN, gameTick);
		}

		public CarPositionContext getCarContextById(string carId) {
			return getCarContextById (carId, timeService.gameTick);
		}

		public CarPositionContext getCarContextById(string carId, int gameTick) {

			if (m_carPosHistory.ContainsKey(gameTick)) {
				List<CarPositionContext> cpcs = m_carPosHistory [gameTick];
				foreach (CarPositionContext cpc in cpcs) {
					if (cpc.name == carId)
						return cpc;
				}
			}  

			return null;
		}

		#endregion
	}
}

