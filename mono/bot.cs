using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using Newtonsoft.Json;
using teamgsn;
using Newtonsoft.Json.Linq;

public class Bot
{
	public static readonly string TAG = typeof (Bot).Name;

	private static readonly string DEFAULT_TRACK_NAME = Track.FINLAND;
	private static readonly int DEFAULT_CAR_COUNT = 1;
	private static readonly string DEFAULT_PASSWORD = null;

	private static BotModule s_mod;

	public static void Main (string[] args)
	{
		string host = args [0];
		int port = int.Parse (args [1]);
		string botName = args [2];
		string botKey = args [3];

		string trackName = DEFAULT_TRACK_NAME;
		int carCount = DEFAULT_CAR_COUNT;
		string password = DEFAULT_PASSWORD;

		if (args.Length > 4)
			trackName = args [4];

		if (args.Length > 5) 
			int.TryParse (args [5], out carCount);

		if (args.Length > 6)
			password = args [6];

		//reset password to null if empty
		password = password == String.Empty ? null : password;

		ISerialize joinMsg;

//		if (args.Length > 4) 
//			joinMsg = new JoinRace (botName, botKey, trackName, carCount, password);
//		else
			joinMsg = new Join (botName, botKey);

		s_mod = BotModule.CreateModule(botName, trackName, carCount, password);

		Log.D(TAG, "Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using (TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream ();
			StreamReader reader = new StreamReader (stream);
			StreamWriter writer = new StreamWriter (stream);
			writer.AutoFlush = true;

			new Bot (reader, writer, joinMsg);
		}
	}

	private StreamWriter writer;

	Bot (StreamReader reader, StreamWriter writer, ISerialize joinRace)
	{
		this.writer = writer;
		string line;

		send (joinRace);

		s_mod.doJoinRace ();

		while ((line = reader.ReadLine()) != null) {

			try {
				GameMsg gm = JsonConvert.DeserializeObject<GameMsg> (line);
				string data = JsonHelper.Serialize (gm.data);

				if (gm.hasGameId)
					s_mod.timeService.gameId = gm.gameId;
				if (gm.hasGameTick)
					s_mod.timeService.gameTick = gm.gameTick;

				switch (gm.msgType) {
				case "carPositions":
//					Console.WriteLine ("cp: " + data);
					List<CarPosition> cp = JsonConvert.DeserializeObject<List<CarPosition>> (data);

					// first carPositions is setup for frame 0
					if (!gm.hasGameTick)
						s_mod.timeService.gameTick = 0;

					s_mod.doCarPositions(cp);

					SwitchLane sl = s_mod.getLane ();
					Throttle t = s_mod.getThrottle ();
					Turbo turbo = s_mod.getTurbo ();

					if (sl.value != SwitchLanes.None) 
						send (sl);
					else if (turbo.value != TurboOptions.None)
						send (turbo);
					else
						send (t);

					break;
				case "join":
					Log.D(TAG, "Joined");
					s_mod.doJoin();
					send (s_mod.getThrottle ());
					break;
				case "yourCar":
					Console.WriteLine ("Your Car: " + data);
					YourCar car = JsonConvert.DeserializeObject<YourCar>(data);
					s_mod.setCarName(car.name);
					send(new Ping());
					break;
				case "gameInit":
					Log.D(TAG, "Race init");
					RootRaceObject raceObj = JsonConvert.DeserializeObject<RootRaceObject>(data);
					Race race = raceObj.race;
					Log.D(TAG, "Race track " + data);
//					Log.D(TAG, "Race track pieces " + race.track.pieces);
					s_mod.doGameInit(race);
					send(new Ping());
					break;
				case "gameEnd":
					Log.D(TAG, "Race ended");
					s_mod.doGameEnd();
					send (new Ping ());
					break;
				case "gameStart":
					Log.D(TAG, "Race starts");
					s_mod.doGameStart();
					send (new Ping ());
					break;
				case "lapFinished":
					Log.D(TAG, "Lap finished ");
					s_mod.doLapFinished();
					send (new Ping ());
					break;
				case "spawn":
					Log.D(TAG, "Spawn: " + data);
					s_mod.doSpawn();
					send (new Ping ());
					break;
				case "crash":
					Log.D(TAG, "Crash: "); //+ data);
					s_mod.doCrash();
					send (new Ping ());
					break;
				case "dnf":
					Log.D(TAG, "Disqualified: " + data);
					s_mod.doDisqualified();
					send (new Ping ());
					break;
				case "turboAvailable":
					Log.D(TAG, "turboAvailable: " + data);
					s_mod.doTurboAvailable();
					send (new Ping ());
					break;
				case "finish":
					Log.D(TAG, "Finish: " + data);
					s_mod.doFinish();
					send (new Ping ());
					break;
				default:
					Log.D(TAG, "Other Type " + gm.msgType + ": " + data);
					send (new Ping ());
					break;
				}
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
			}

			// OTHER MESSAGES SEEN!
			// turboAvailable: {"turboDurationMilliseconds":500.0,"turboDurationTicks": 30,"turboFactor":3.0}
		}
	}

	private void send (ISerialize msg)
	{
		string msgStr = msg.Serialize (s_mod.gameTick);
		writer.WriteLine (msgStr);
	}
}
